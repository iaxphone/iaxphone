#ifndef CL_ASSERT_H
#define CL_ASSERT_H

extern int (*abort_function)(const char*, const char*, int);

int abort_function_c(const char *s, const char *file, int line);
int set_abort_function(int (*func)(const char*, const char*, int));

#ifndef CL_ABORT
#include <assert.h>
#else
#define assert(e) ( (e) || abort_function==NULL ? ((void)0) : abort_function(#e, __FILE__, __LINE__));
#endif

#endif
