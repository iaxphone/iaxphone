#include <stdio.h>
#include <stdlib.h>
#include "assert.h"

#include "iax_basis.h"

#include "iax-client.h"

#include "iax_pack.h"

#define MAX_SESSIONS 4
#define MAX_NB_BYTES 512

#ifdef SPEEX_ENABLE
#include <speex/speex.h>
#endif

#ifdef ILBC_ENABLE
#include <iLBC_encode.h>
#include <iLBC_decode.h>
#define USE_ILBC_ENHANCER 0
#define ILBC_MS 30
//#define ILBC_MS 20
#endif

#ifdef GSM_ENABLE
#include <gsm.h>
#endif

#include "phonesup.h"

struct thread_pack thr;
HANDLE Process_Thread;

struct peer {
	int time;

#ifdef GSM_ENABLE
	gsm gsmin;
	gsm gsmout;
#endif

#ifdef SPEEX_ENABLE
  SpeexBits speexin, speexout;
  void *enc_state, *dec_state;
  int frame_size;
#endif
  
#ifdef ILBC_ENABLE
  iLBC_Enc_Inst_t enc;
  iLBC_Dec_Inst_t dec;
#endif

  struct iax_session *session;
  struct peer *next;

};

struct encode_pack {
  int frame_size, samples;
  char *dataBytes;
};

int (*encode_function)(char *wData, struct peer *p, struct encode_pack *e, void *extra);

static struct peer *peers;
static int answered_call = 0;

static struct peer *most_recent_answer;

struct iax_session *most_recent_session() {

  assert(most_recent_answer != NULL);

  return most_recent_answer->session;

}

struct iax_session *newcall;

#include "audio_base.h"

#ifdef WIN32
#include "winsound1.h"
#endif

struct rosen_pack rp;

void service_network(int netfd, FILE *f, struct iax_pack *i, struct audio_base *a);

int sn_callback(void *extra, struct audio_base *a) {

  struct rosen_pack *r = (struct rosen_pack *) extra;

  assert(extra!=NULL);

  service_network(r->netfd, r->f, r->i, a);

  return 0;

}

int base_output(char *s, int len, void *extra __attribute__((unused))) {
  assert(s!=NULL && len>0);
  write(1, s, len);
  return 0;
}

int empty_output(char *s __attribute__((unused)), 
		 int len __attribute__((unused)), 
		 void *extra __attribute__((unused))) {

  return 0;

}

int (*stroutput)(char *s, int len, void *extra) = base_output;

int set_stroutput(int (*o)(char*,int,void*)) {

  stroutput = o;

  return 0;

}

#include <stdarg.h>

int wfprintf(FILE *f, char *s, ...) {

  char buf[512];
  int retval;
  va_list ap;

  assert(s!=NULL);

  buf[0] = 0;

  memset(buf, 0, sizeof(buf));

  va_start(ap, s);
  retval = vsprintf(buf, s, ap);
  va_end(ap);

  if (retval>0) 
    stroutput(buf, strlen(buf), f);

  return 0;

}

int enc_callback(void *extra, struct audio_base *a, unsigned long *longlasttick, int i) {

  struct encode_pack e;
  void *encoding_extra;

  struct rosen_pack *r = (struct rosen_pack *) extra;

#if defined(SPEEX_ENABLE) || defined(ILBC_ENABLE)
  char byte_ptr[MAX_NB_BYTES];
  int nbBytes;
#endif

#ifdef GSM_ENABLE
  gsm_frame fo;
#endif

  assert(extra!=NULL && longlasttick != NULL);

  switch (r->i->selected_format) {

#ifdef GSM_ENABLE
  case AST_FORMAT_GSM:
    if(!most_recent_answer->gsmout)
      most_recent_answer->gsmout = gsm_create();
    
    encoding_extra = &fo;
    break;
#endif

#ifdef SPEEX_ENABLE
  case AST_FORMAT_SPEEX:
    e.frame_size = most_recent_answer->frame_size;
    e.dataBytes = byte_ptr;
    break;
#endif

#ifdef ILBC_ENABLE
  case AST_FORMAT_ILBC:
    e.dataBytes = byte_ptr;
    break;
#endif

  default: wfprintf(r->f, "Never matched a selected encoding format"); return -1;

  };

  service_network(r->netfd,r->f,r->i,a); /* service network here for better performance */

  encode_function(a->fetch_wdata(i), most_recent_answer, &e, encoding_extra);
		
  if(r->i->iax_send_voice(most_recent_answer->session,
			   r->i->selected_format, (char *) e.dataBytes, e.frame_size, e.samples) == -1) {
		  
    wfprintf(r->f,"Failed to send voice!"); 
		  
  }

  *longlasttick = GetTickCount(); /* save time of last output */

  return 0;
		
}

static struct peer *find_peer(struct iax_session *session)
{
	struct peer *cur = peers;
	while(cur) {
		if (cur->session == session)
			return cur;
		cur = cur->next;
	}
	return NULL;
}

void
issue_prompt(FILE *f)
{
	wfprintf(f, "TeleClient> ");
}

void
answer_call(struct iax_pack *i, FILE *f)
{
  assert(i!=NULL);

  if(most_recent_answer)
    i->iax_answer(most_recent_answer->session);

  wfprintf(f, "Answering call!\n");
  answered_call = 1;

}

int parse_args(FILE *f, unsigned char *cmd, struct iax_pack *i, struct audio_base *a);

int handle_event(FILE *f, struct iax_event *e, struct peer *p, struct iax_pack *i, struct audio_base *a) {

	int len,retval,n=0;
	short fr[SZE];
	static paused_xmit = 0;

#if defined(SPEEX_ENABLE) || defined (ILBC_ENABLE)
	float output_frame[SZE];
#endif

	assert(a!=NULL);

	switch(e->etype) {
		case IAX_EVENT_HANGUP:
			i->iax_hangup(most_recent_answer->session, "Byeee!");
			wfprintf(f, "Call disconnected by peer\n");
			free(most_recent_answer);
			most_recent_answer = 0;
			answered_call = 0;
			peers = 0;
			newcall = 0;
			
			break;

		case IAX_EVENT_REJECT:
			wfprintf(f, "Authentication was rejected\n");
			break;
		case IAX_EVENT_ACCEPT:
			wfprintf(f, "Waiting for answer... RING RING\n");
			issue_prompt(f);
			break;
		case IAX_EVENT_ANSWER:
			answer_call(i,f);
 			break;
		case IAX_EVENT_VOICE:

		  switch(e->subclass) {

		    //	case AST_FRAME_VOICE:
		    //	break;

#ifdef ILBC_ENABLE
		  case AST_FORMAT_ILBC:

		    if (e->datalen == 0) {

			iLBC_decode(output_frame, NULL, &p->dec, 0);

			break;
			
		    }

		    if (e->datalen % 50) break;

		    for (len = 0; len < e->datalen; len+=50) {

		      iLBC_decode(output_frame, e->data + len, &p->dec, 1);

		      }

		    if (a->throwaway_check != NULL) {

		      if (a->throwaway_check(a,&n)) break;

		    }

		      conv_float_samples(output_frame, 240, (char*)fr, 240*sizeof(short));
		      a->out_audio(a->extra,n,fr,240*sizeof(short),&paused_xmit);

		    break;
#endif

#ifdef SPEEX_ENABLE
		  case AST_FORMAT_SPEEX:

		    len = 0;
		    while (len < e->datalen) {
		      
		      speex_bits_read_from(&p->speexin, e->data+len, e->datalen);
		      retval = speex_decode(p->dec_state, &p->speexin, output_frame);
		      
		      len += 160;

		    }

		    if (a->throwaway_check != NULL) {

		      if (a->throwaway_check(a,&n)) break;

		    }

		    conv_float_samples(output_frame, 160, (char*)fr, 320);
		    a->out_audio(a->extra,n,fr,160*sizeof(short),&paused_xmit);

		    break;
#endif

#ifdef GSM_ENABLE
		  case AST_FORMAT_GSM:
		    
		    if (e->datalen % 33) {
		      wfprintf(stderr, "Weird gsm frame, not a multiple of 33.\n");
		      break;
		    }

		    if (!p->gsmin)
		      p->gsmin = gsm_create();
		    
		    len = 0;
		    while(len < e->datalen) {
		      if(gsm_decode(p->gsmin, (char *) e->data + len, fr)) {
			wfprintf(stderr, "Bad GSM data\n");
			break;
		      } else {  /* its an audio packet to be output to user */
			

			if (a->throwaway_check != NULL) {

			  if (a->throwaway_check(a,&n)) break;

			}

			if (a->out_audio(a->extra,n,fr,160*sizeof(short),&paused_xmit) == -1) return -1;

#ifdef	PRINTCHUCK
			else printf("Chucking packet!!\n");
#endif
		      }
		      len += 33;
		    }
		    break;
#endif

		  default :
		    wfprintf(f, "Don't know how to handle that format %d\n", e->subclass);
		  }
		  
		  break;

		case IAX_EVENT_RINGA:
			break;
		default:
			wfprintf(f, "Unknown event: %d\n", e->etype);
			break;
	}
}

void
do_iax_event(FILE *f, struct iax_pack *i, struct audio_base *a) {
	int sessions = 0;
	struct iax_event *e = 0;
	struct peer *peer;

	assert(i!=NULL);

	while ( (e = i->iax_get_event(0))) {
		peer = find_peer(e->session);
		if(peer) {
		  handle_event(f, e, peer, i, a);
		} else {
		  if(e->etype != IAX_EVENT_CONNECT) {
				wfprintf(stderr, "Huh? This is an event for a non-existant session?\n");
			}
			sessions++;

			if(sessions >= MAX_SESSIONS) {
				wfprintf(f, "Missed a call... too many sessions open.\n");
			}

			if(e->session->callerid[0] && e->session->dnid[0])
				wfprintf(f, "Call from '%s' for '%s'", e->session->callerid,
				e->session->dnid);
			else if(e->session->dnid[0]) {
				wfprintf(f, "Call from '%s'", e->session->dnid);
			} else if(e->session->callerid[0]) {
				wfprintf(f, "Call from '%s'", e->session->callerid);
			} else printf("Call from");
			wfprintf(f, " (%s)\n", inet_ntoa(i->iax_get_peer_addr(e->session).sin_addr));

			if(most_recent_answer) {
				wfprintf(f, "Incoming call ignored, there's already a call waiting for answer... \
please accept or reject first\n");
				i->iax_reject(e->session, "Too many calls, we're busy!");
			} else {
				if ( !(peer = malloc(sizeof(struct peer)))) {
					wfprintf(f, "Warning: Unable to allocate memory!\n");
					return;
				}

				peer->time = time(0);
				peer->session = e->session;

#ifdef GSM_ENABLE
				peer->gsmin = 0;
				peer->gsmout = 0;
#endif

				peer->next = peers;
				peers = peer;

				i->iax_accept(peer->session, i->selected_format);
				i->iax_ring_announce(peer->session);
				most_recent_answer = peer;
				wfprintf(f, "Incoming call!\n");
			}
			i->iax_event_free(e);
			issue_prompt(f);
		}
	}
}

int process_audioin_prepare(int netfd, FILE *f, struct iax_pack *ipack, struct audio_base *a) {

  struct rosen_pack r = { netfd, f, ipack };

  assert(ipack != NULL);

  return a->process_insound_prep != NULL ? a->process_insound_prep(a, sn_callback, &r) : 0;

}

#ifdef GSM_ENABLE
int encode_function_gsm(char *wData, struct peer *p, struct encode_pack *e, void *extra) {

  gsm_frame *fo = (gsm_frame*) (extra);

  assert(p!=NULL && e!=NULL);

  /* encode the audio from the buffer into GSM format */

  e->frame_size = sizeof(gsm_frame);
  e->samples = 33;
  e->dataBytes = (char*) *fo;

  gsm_encode(p->gsmout, (short *) wData, *fo);

  return 0;

}
#endif

#ifdef SPEEX_ENABLE

int conv_float_samples(float *r, int num, char *dataBytes, int size) {

  float *e = r + num;
  signed short *w = (signed short*) dataBytes;

  assert(w != NULL && size > 0 && dataBytes != NULL && num > 0);

  while ( r < e ) {

    *w++ = *r++;

  }
  
  return 0;

}

int conv_samples_float(char *dataBytes, int num, float *w, int size) {

  signed short *s = (signed short *) dataBytes, *e = s + size;

  assert(dataBytes != NULL && num > 0 && w != NULL && size > 0);

  while ( s < e ) {

    *w++ = *s++;

  }
  
  return 0;

}

int encode_function_speex(char *wData, struct peer *p, struct encode_pack *e, void *extra) {

  float samples[160];

  assert(p!=NULL && e!=NULL);

  conv_samples_float(wData, 320, samples, 160);

  speex_bits_reset(&p->speexout);
  speex_encode(p->enc_state, samples, &p->speexout);
  e->samples = speex_bits_write(&p->speexout, e->dataBytes, MAX_NB_BYTES);

  return 0;

}
#endif

#ifdef ILBC_ENABLE
int encode_function_ilbc(char *wData, struct peer *p, struct encode_pack *e, void *extra) {

  float samples[240];
  char dataBytes[480];
  int retval;

  assert(p!=NULL && e!=NULL && e->dataBytes != NULL);

  conv_samples_float(wData, 480, samples, 240);

  iLBC_encode(e->dataBytes, samples, &p->enc);

  e->frame_size = 50;
  e->samples = 50;

  return 0;

}
#endif

int set_encode_function(int format) {

  int (*func)(char*,struct peer*,struct encode_pack*,void*) = NULL;

  switch(format) {
#ifdef GSM_ENABLE
  case AST_FORMAT_GSM: func = encode_function_gsm; break;
#endif

#ifdef SPEEX_ENABLE
  case AST_FORMAT_SPEEX: func = encode_function_speex; break;
#endif

#ifdef ILBC_ENABLE
  case AST_FORMAT_ILBC: func = encode_function_ilbc; break;
#endif
  };

  encode_function = func;

  return 0;

}

int process_audioin_process(int netfd, FILE *f, struct iax_pack *ipack, struct audio_base *a) {

  static unsigned long lastouttick = 0;
  struct rosen_pack r = { netfd, f, ipack };

  assert(ipack!=NULL);

  return a->process_insound != NULL ? a->process_insound(a, sn_callback, enc_callback, &r, &lastouttick, answered_call) : 0;

}

int process_outqueue(int netfd, FILE *f, struct iax_pack *ipack, struct audio_base *a) {

  struct rosen_pack r = { netfd, f, ipack };

  assert(ipack!=NULL);

  return a->process_outsound != NULL ? a->process_outsound(a, sn_callback, &r) : 0;
  
}

char *iax_pack_strings[] = { "iax_init", "iax_get_event",
			     "iax_reject", "iax_accept",
		      "iax_ring_announce", "iax_event_free",
		      "iax_time_to_next_event", "iax_hangup",
		      "iax_answer", "iax_session_new",
			     "iax_call", "iax_send_dtmf", "iax_get_peer_addr",
			     "iax_send_voice", "iax_destroy", "iax_get_fd" };

char *phone_pack_strings[] = { "service_network", "process_outqueue",
		      "process_audioin_prepare", "process_audioin_process",
		      "create_thread", "resume_thread", "parse_args", "issue_prompt",
			       "initial_audio" };

char *audio_base_strings[] = { "init_audio", "out_audio", "throwaway_check",
			       "block_audio", "process_outsound", "process_insound_prep", "process_insound", "fetch_wdata",
			       "close_audio" };

int num_iax_pack_strings() {
  return sizeof(iax_pack_strings) / sizeof(char*);
}

int num_phone_pack_strings() {
  return sizeof(phone_pack_strings) / sizeof(char*);
}

int num_audio_base_strings() {
  return sizeof(audio_base_strings) / sizeof(char*);
}

int phonesup_init() {

  thr.state = 0;

#ifdef WIN32
  WORD wVersionRequested;
  WSADATA wsaData;
  int err;
 
  wVersionRequested = MAKEWORD( 1, 1 );
 
  err = WSAStartup( wVersionRequested, &wsaData );

  if (err != 0) return -1;
#endif

  return 0;

}

int phonesup_cleanup() {

#ifdef WIN32
  WSACleanup();
#endif

  return 0;

}

#ifdef DEBUG_IAX_PACK
int show_iax_pack(FILE *f, struct iax_pack *i) {

    char **s = iax_pack_strings, 
      **e = s + sizeof(iax_pack_strings) / sizeof(char*);

    void **p;

    assert(i!=NULL);

    p = &i->iax_init;

    wfprintf(f, "iax_init = %p\n", i->iax_init);
    wfprintf(f, "iax_get_fd = %p\n", i->iax_get_fd);

    for ( ; s < e; s++, p++) {

      wfprintf(f, "%s = %p, value = %p\n", *s, *p, p);

    }

    wfprintf(f, "netfd = %d\n", i->netfd);
   
  return 0;

}
#endif

#ifdef SPEEX_ENABLE
int init_speex_work(struct peer *p) {

  int quality = 8, sampling_rate = 8000, bitrate = 28000;

  assert(p!=NULL);

  speex_bits_init(&p->speexout);
  p->enc_state = speex_encoder_init(&speex_nb_mode);

  speex_encoder_ctl(p->enc_state, SPEEX_SET_QUALITY, &quality);
  speex_encoder_ctl(p->enc_state, SPEEX_SET_SAMPLING_RATE, &sampling_rate);
  speex_encoder_ctl(p->enc_state, SPEEX_SET_BITRATE, &bitrate);
  speex_encoder_ctl(p->enc_state, SPEEX_GET_FRAME_SIZE, &p->frame_size);

  speex_bits_init(&p->speexin);
  p->dec_state = speex_decoder_init(&speex_nb_mode);

  return 0;

}
#endif

#ifdef ILBC_ENABLE
int init_ilbc_work(struct peer *p) {

  assert(p!=NULL);

  memset(&p->enc, 0, sizeof(p->enc));
  initEncode(&p->enc, ILBC_MS);
  memset(&p->dec, 0, sizeof(p->dec));
  initDecode(&p->dec, ILBC_MS, USE_ILBC_ENHANCER);

  return 0;

}
#endif

int selected_workinit(int format, struct peer *p, struct audio_base *a) {

  switch(format) {

#ifdef SPEEX_ENABLE
  case AST_FORMAT_SPEEX:
	init_speex_work(p);
	a->frame_size = 160;
	break;

#endif

#ifdef GSM_ENABLE
  case AST_FORMAT_GSM:
	p->gsmin = 0;
	p->gsmout = 0;
	a->frame_size = 160;
	break;
#endif

#ifdef ILBC_ENABLE
  case AST_FORMAT_ILBC:
	init_ilbc_work(p);
	a->frame_size = 240;
	break;
#endif
  }

	return 0;

}

void
call(FILE *f, char *num, char *ich, struct iax_pack *i, struct audio_base *a)
{
	struct peer *peer;

	assert(i!=NULL && ich!=NULL);

	if(!newcall) {
		newcall = i->iax_session_new();
	}
	else {
		wfprintf(f, "Already attempting to call somewhere, please cancel first!\n");
		return;
	}

	if ( !(peer = malloc(sizeof(struct peer)))) {
		wfprintf(f, "Warning: Unable to allocate memory!\n");
		return;
	}

	peer->time = time(0);
	peer->session = newcall;

	peer->next = peers;
	peers = peer;

	most_recent_answer = peer;

	selected_workinit(i->selected_format, peer, a);

	i->iax_call(peer->session, num, NULL, ich, NULL, 0, i->selected_format, i->capabilities);

	set_encode_function(i->selected_format);

}

void
reject_call(struct iax_pack *i)
{
  assert(i!=NULL);

	i->iax_reject(most_recent_answer->session, "Call rejected manually.");
	most_recent_answer = 0;
}

void
dump_call(struct iax_pack *i)
{
  assert(i!=NULL);

	if(most_recent_answer)
	{
		i->iax_hangup(most_recent_answer->session,"");
		free(most_recent_answer);
	}
	printf("Dumping call!\n");
	answered_call = 0;
	most_recent_answer = 0;
	answered_call = 0;
	peers = 0;
	newcall = 0;
}

int sample_out() {
  stroutput("Sample\n", 7, NULL);
  return 0;
}

#ifdef WIN32
DWORD WINAPI ProcessThread(LPVOID lpParam) {

  struct thread_pack *thr = (struct thread_pack*) lpParam;
  struct audio_base *a;
  struct rosen_pack *r;
  char *s;

  int (*previous_output)(char *s, int len, void *extra __attribute__((unused))) = stroutput;

  DWORD result;

  assert(thr != NULL);

  r = thr->r;
  a = thr->a;

  while ( thr->state & tr_process ) {

    result = a->block_audio(a, 3);

    stroutput = empty_output;

    switch (result) {
    case WAIT_FAILED: s="WAIT_FAILED"; return;
    case WAIT_TIMEOUT: s="WAIT_TIMEOUT";
    case WAIT_ABANDONED_0: s="WAIT_ABONDONED_0";
    case WAIT_ABANDONED_0+1: s="WAIT_ABANDONED_0+1";
    case WAIT_OBJECT_0:  s="WAIT_OBJECT_0";
    case WAIT_OBJECT_0+1: s="WAIT_OBJECT_0+1";

      process_audioin_prepare(r->netfd, r->f, r->i, a);
      process_audioin_process(r->netfd, r->f, r->i, a);
      process_outqueue(r->netfd, r->f, r->i, a); 
      break;

    default: s=NULL;

    }

    service_network(r->netfd, r->f, r->i, a);

    stroutput = previous_output;

  }

  stroutput = empty_output;

  if (most_recent_answer) {
    r->i->iax_destroy(most_recent_answer->session);
  }

  thr->a->close_audio(thr->a);

  stroutput = previous_output;

  return 0;

}
#endif

int create_thread(int netfd, FILE *f, struct iax_pack *i, struct audio_base *a) {

  assert(i!=NULL);

  rp.netfd = netfd;
  rp.f = f;
  rp.i = i;

  thr.r = &rp;
  thr.a = a;
  thr.state = tr_process;

  if ( (Process_Thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) ProcessThread, (LPVOID) &thr, 
				      CREATE_SUSPENDED, &thr.dwThreadId)) == NULL) return -1;

  return 0;

}

int resume_thread() {

  ResumeThread(Process_Thread);

}

int suspend_thread() {
      
  SuspendThread(Process_Thread);

}

int stat_thread() {

  DWORD ExitCode;
  
  if (!GetExitCodeThread(Process_Thread, &ExitCode)) return -1;

  if (ExitCode == STILL_ACTIVE) return 1;

  return 0;

}

int initial_audio(int netfd, FILE *f, struct iax_pack *i, struct audio_base *a) {

  assert(i!=NULL);

  rp.netfd = netfd;
  rp.f = f;
  rp.i = i;

  a->frame_size = 160;

  if (a->init_audio(a, &rp) == -1) return -1;
  
  return 0;

}

int parse_cmd(FILE *f, int argc, char **argv, struct iax_pack *i, struct audio_base *a)
{
	_strupr(argv[0]);
	if(!strcmp(argv[0], "HELP")) {
	  /*
		if(argc == 1)
			dump_array(f, help);
		else if(argc == 2) {
			if(!strcmp(argv[1], "HELP"))
				wfprintf(f, "Help <Command>\t-\tDisplays general help or specific help on command if supplied an arguement\n");
			else if(!strcmp(argv[1], "QUIT"))
				wfprintf(f, "Quit\t\t-\tShuts down the miniphone\n");
			else wfprintf(f, "No help available on %s\n", argv[1]);
		} else {
			wfprintf(f, "Too many arguements for command help.\n");
		}
*/
	} else if(!strcmp(argv[0], "STATUS")) {
		if(argc == 1) {
			int c = 0;
			struct peer *peerptr = peers;

			if(!peerptr)
				wfprintf(f, "No session matches found.\n");
			else while(peerptr) {
	 			wfprintf(f, "Listing sessions:\n\n");
				wfprintf(f, "Session %d\n", ++c);
				wfprintf(f, "Session existed for %d seconds\n", (int)time(0)-peerptr->time);
				if(answered_call)
					wfprintf(f, "Call answered.\n");
				else wfprintf(f, "Call ringing.\n");

				peerptr = peerptr->next;
			}
		} else wfprintf(f, "Too many arguments for command status.\n");
	} else if(!strcmp(argv[0], "ANSWER")) {
		if(argc > 1)
			wfprintf(f, "Too many arguements for command answer\n");
		else answer_call(i,f);
	} else if(!strcmp(argv[0], "REJECT")) {
		if(argc > 1)
			wfprintf(f, "Too many arguements for command reject\n");
		else {
			wfprintf(f, "Rejecting current phone call.\n");
			reject_call(i);
		}
	} else if(!strcmp(argv[0], "CALL")) {

		if(argc > 2)
			wfprintf(f, "Too many arguements for command call\n");
		else {
			call(f, "12345", argv[1], i, a);
		}
	} else if(!strcmp(argv[0], "DUMP")) {
		if(argc > 1)
			wfprintf(f, "Too many arguements for command dump\n");
		else {
			dump_call(i);
		}
	} else if(!strcmp(argv[0], "DTMF")) {
		if(argc > 2)
		{
			wfprintf(f, "Too many arguements for command dtmf\n");
			return;
		}
		if (argc < 1)
		{
			wfprintf(f, "Too many arguements for command dtmf\n");
			return;
		}
		if(most_recent_answer)
				i->iax_send_dtmf(most_recent_answer->session,*argv[1]);
	} else if(!strcmp(argv[0], "QUIT")) {
		if(argc > 1)
			wfprintf(f, "Too many arguements for command quit\n");
		else {

		  wfprintf(f, "Good bye!\n");

		  if (thr.state==tr_process) {

		    thr.state = 0;

		    ResumeThread(Process_Thread);

		    wfprintf(f, "Waiting for thread to die.\n");

#ifdef WIN32
		    WaitForMultipleObjects(1, &Process_Thread, TRUE, INFINITE);
#endif

		    wfprintf(f, "Finished wait.\n");

		  }

		  return PS_QUIT;

		}
	} else wfprintf(f, "Unknown command of %s\n", argv[0]);
}

int parse_args(FILE *f, unsigned char *cmd, struct iax_pack *i, struct audio_base *a) {

	static char *argv[MAXARGS];
	unsigned char *parse = cmd;
	int argc = 0, t = 0;

	assert(cmd != NULL);

	memset(argv, 0, sizeof(argv));
	while(*parse) {
		if(*parse < 33 || *parse > 128) {
			*parse = 0, t++;
			if(t > MAXARG) {
				wfprintf(f, "Warning: Argument exceeds maximum argument size, command ignored!\n");
				return -1;
			}
		} else if(t || !argc) {
			if(argc == MAXARGS) {
				wfprintf(f, "Warning: Command ignored, too many arguments\n");
				return -1;
			}
			argv[argc++] = parse;
			t = 0;
		}

		parse++;
	}

	if(argc)
		return parse_cmd(f, argc, argv, i, a);

	wfprintf(f, "Leaving parse-args.\n");

	return 0;

}

void service_network(int netfd, FILE *f, struct iax_pack *i, struct audio_base *a)
{
	fd_set readfd;
	struct timeval dumbtimer;

	assert(i!=NULL && a!=NULL);

	/* set up a timer that falls-through */
	dumbtimer.tv_sec = 0;
	dumbtimer.tv_usec = 0;


           		for(;;) /* suck everything outa network stuff */
		{
			FD_ZERO(&readfd);
			FD_SET(netfd, &readfd);
			if (select(netfd + 1, &readfd, 0, 0, &dumbtimer) > 0)
			{
				if (FD_ISSET(netfd,&readfd))
				{
				  do_iax_event(f,i,a);
					(void) i->iax_time_to_next_event();
				} else break;
			} else break;
		}
	       	do_iax_event(f,i,a); /* do pending event if any */
}



