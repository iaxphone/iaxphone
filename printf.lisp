(in-package #:cl-user)

(defpackage printf 
   (:use common-lisp) 
   (:export printf->format)) 

(in-package #:printf)

(defvar *string-escape-character* #\\) 

(defvar *string-escape-table* 
   ;; built with LIST and CONS so it's legal to modify it 
   (list 
    (cons #\a #\Bell) 
    (cons #\b #\Backspace) 
    (cons #\f #\Page) 
    (cons #\n #\Newline) 
    (cons #\r #\Return) 
    (cons #\t #\Tab))) 


;;; Acclaim/Blame for this one goes to Garreth .. 
(set-dispatch-macro-character #\# #\" 
   (lambda (stream sub-char infix-arg) 
     (declare (ignore sub-char infix-arg)) 
     (let ((delimiter #\") 
           (escaped nil)) 
       (coerce (loop for next-char = (read-char stream) nconc 
                     (cond 
                      (escaped 
                       (progn (setf escaped nil) 
                         (list (or (cdr (assoc next-char   
*string-escape-table*)) 
                                   next-char)))) 
                      ((char= next-char delimiter) 
                       (loop-finish)) 
                      ((char= next-char *string-escape-character*) 
                       (setf escaped t) 
                       nil) 
                      (t (list next-char)))) 
               'string)))) 


(in-package printf) 


(define-condition printf-error (error) 
   ((ch :reader ch :initarg :ch)) 
   (:report (lambda (condition stream) 
             (format stream "'~A' is not valid in a printf format" (ch   
condition))))) 


;;; Format of printf %[Flags][Width].[Precition][Size][Type] 
(defvar *digits* '(#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9)) 
(defvar *flags* '(#\- #\+ #\# #\Space)) 
(defvar *size* '(#\h #\l #\L)) 
(defvar *sign* '(#\+ #\-)) 


(defmacro remember ((name value) calling (function &rest args)) 
   (assert (eq calling :calling)) 
   `(lambda ,args 
      (let ((,name ,value)) 
      (declare (special ,name)) 
      (,function ,@args)))) 


(defmacro aif (test-form then-form &optional else-form) 
   "From Paul Graham: On Lisp - Anamorphic macroes" 
   `(let ((it ,test-form)) 
      (if it ,then-form ,else-form))) 


(defun char-dispatch (flag width precition size) 
   (declare (ignore flag width precition size)) 
   "~C") 


(defun integer-dispatch (flag width precition size) 
   "~<@><mincol>,<padchar>,<comma-char><comma-interval>" 
   (declare (ignore size) (special type)) 
   (let ((modifier (if (and flag (char= flag #\+)) #\@ nil)) (padchar (if   
precition #\0 nil))) 
     (with-output-to-string (ostream) 
       (if (and flag (char= flag #\-)) 
           (progn ; left justify 
             (write-char #\~ ostream) 
             (when width (write-string width ostream)) 
             (write-string "@<~" ostream) 
             (write-char type ostream) 
             (write-string "~>" ostream)) 
         (progn 
           (write-char #\~ ostream) 
           (when width (write-string width ostream)) 
           (when padchar 
             (write-string ",'" ostream) 
             (write-char padchar ostream)) 
           (when modifier (write-char modifier ostream)) 
           (write-char type ostream)))))) 


(defun ignore-dispatch (flag width precition size) 
   (declare (ignore flag width precition size))) 


(defun float-dispatch (flag width precition size) 
   "~w,d,k,overflowchar,padchar" 
   (declare (ignore size) (special type)) 
   (let ((w width) (d (if precition precition "6"))) 
     (with-output-to-string (ostream) 
       (if (and flag (char= flag #\-)) 
           (progn ; left justify 
             (write-char #\~ ostream) 
             (when width (write-string width ostream)) 
             (write-string "@<~" ostream) 
             (write-char #\, ostream) 
             (write-string d ostream) 
             (write-char type ostream) 
             (write-string "~>" ostream)) 
         (progn 
           (write-char #\~ ostream) 
           (when width (write-string w ostream)) 
           (write-char #\, ostream) 
           (write-string d ostream) 
           (write-char type ostream)))))) 


(defun string-dispatch (flag width precition size) 
   "~mincol,colinc,minpad,padcharA" 
   (declare (ignore precition size)) 
   (with-output-to-string (ostream) 
     (write-char #\~ ostream) 
     (when width (write-string width ostream)) 
     (when (and flag (char= flag #\-)) (write-char #\@ ostream)) 
     (write-string "<~A~>" ostream))) 


(defun identity-dispatch (flag width precition size) 
   (declare (ignore flag precition width size)) 
   "%") 


(defparameter *type-dispatcher* 
   (list 
    (cons #\c #'char-dispatch) 
    (cons #\d (remember (type #\D) :calling (integer-dispatch flag width   
precition size))) 
    (cons #\e (remember (type #\E) :calling (float-dispatch flag width   
precition size))) 
    (cons #\E (remember (type #\E) :calling (float-dispatch flag width   
precition size))) 
    (cons #\f (remember (type #\F) :calling (float-dispatch flag width   
precition size))) 
    (cons #\g (remember (type #\G) :calling (float-dispatch flag width   
precition size))) 
    (cons #\G (remember (type #\G) :calling (float-dispatch flag width   
precition size))) 
    (cons #\i (remember (type #\D) :calling (integer-dispatch flag width   
precition size))) 
    (cons #\n #'ignore-dispatch) 
    (cons #\o (remember (type #\O) :calling (integer-dispatch flag width   
precition size))) 
    (cons #\p (remember (type #\X) :calling (integer-dispatch flag width   
precition size))) 
    (cons #\u (remember (type #\D) :calling (integer-dispatch flag width   
precition size))) 
    (cons #\s #'string-dispatch) 
    (cons #\x (remember (type #\X) :calling (integer-dispatch flag width   
precition size))) 
    (cons #\X (remember (type #\X) :calling (integer-dispatch flag width   
precition size))) 
    (cons #\% #'identity-dispatch))) 


(declaim (inline read-int-string-i)) 
(defun read-int-string-i (istream) 
   (with-output-to-string (ostream) 
     (when (member (peek-char nil istream) *sign*) 
       (write-char (read-char istream) ostream)) 
     (loop for char = (read-char istream) until (not (member char *digits*)) 
           do (write-char char ostream) 
           finally (unread-char char istream)))) 


(defun read-int-string (istream) 
   (let ((it (read-int-string-i istream))) 
     (if (= (length it) 0) 
         nil 
       it))) 


(defun printf->format (format) 
   (with-output-to-string (ostream) 
     (with-input-from-string (istream format) 
       (loop with flag = nil 
             with width = nil 
             with precition = nil 
             with size = nil 
             for char = (read-char istream nil 'EOF) until (not (characterp   
char)) do 
             (if (char= char #\%) 
                 (progn 
                   (when (member (peek-char nil istream) *flags*) 
                     (setf char (read-char istream)) 
                     (setf flag char)) 
                   (setf width (read-int-string istream)) 
                   (setf char (read-char istream)) 
                   (when (char= char #\.) 
                     (setf precition (read-int-string istream)) 
                     (setf char (read-char istream))) 
                   (when (member char *size*) 
                     (setf size char) 
                     (setf char (read-char istream))) 
                   (aif (assoc char *type-dispatcher*) 
                        (write-string (funcall (cdr it) flag width   
precition size) ostream) 
                        (error 'printf-error :ch char)) 
                   (setf flag nil width nil precition nil size nil)) 
               (princ char ostream)))))) 


(define-compiler-macro printf->format (&whole whole format) 
     (if (stringp format) 
         (printf->format format) 
       whole)) 


