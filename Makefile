PROGRAM = iaxphone

WIN_LIBS = -lws2_32 -lwinmm
LIBS += $(WIN_LIBS)

CFLAGS = -g -O

OBJS = phonesup.o

DEFINES += -DCL_ABORT
OBJS += assert.o
PROGRAM_OBJS += assert.o

$(PROGRAM) : $(PROGRAM).o ${PROGRAM_OBJS} phonesup.dll
	$(CC) $(PROGRAM).o $(PROGRAM_OBJS) -o $(PROGRAM) $(CFLAGS) $(LDFLAGS) $(LIBS)

LIBPTH = /c/code/library

IAXPTH = $(LIBPTH)/libiax2/src
GSMPTH = $(LIBPTH)/gsm-1.0-pl10
#SPEEXPTH = $(LIBPTH)/speex-1.1.12
SPEEXPTH = $(LIBPTH)/speex-1.0.5
ILBCPTH = $(LIBPTH)/ilbc

INCLUDES+=-I$(IAXPTH)

LDFLAGS += -L$(GSMPTH)/lib
LIBS += -lgsm
INCLUDES+=-I$(GSMPTH)/inc
DEFINES+=-DGSM_ENABLE

LDFLAGS += -L$(SPEEXPTH)/libspeex/.libs
LIBS += -lspeex
INCLUDES+=-I$(SPEEXPTH)/include
DEFINES += -DSPEEX_ENABLE

LDFLAGS += -L$(ILBCPTH)
LIBS += -lilbc
INCLUDES += -I$(ILBCPTH)
DEFINES += -DILBC_ENABLE

DEFINES += -DWIN32
LIBS += -lws2_32

#READLINE_INCLUDE=-I/c/mingw/include
#READLINE_LIB=-L/c/mingw/lib
#INCLUDES+=$(READLINE_INCLUDE)
#LIBS+=$(READLINE_LIB)

LIBS+=-lreadline

#PROGRAM_FILES="/c/Program Files"
#DXPTH = $(PROGRAM_FILES)"/Microsoft DirectX 9.0 SDK (August 2005)"
#INCLUDES += -I$(DXPTH)/Include
#LIBS += $(DXPTH)/lib/x86/dxguid.lib -lgdi32 -luser32

#DEFINES+=-DWINSOUND2
#ALL_CFLAGS+=-include winsound2.h
#OBJS += winsound2.o

DEFINES+=-DWINSOUND1
ALL_CFLAGS+=-include winsound1.h
OBJS += winsound1.o

ALL_CFLAGS = $(CFLAGS) $(DEFINES) $(LDFLAGS)

%.o : %.c
	$(CC) -c $< $(ALL_CFLAGS) $(INCLUDES) -o $*.o

strip :
	strip phonesup.dll
	strip $(PROGRAM).exe

phonesup.o : phonesup.h

iaxphone.o : phonesup.o

clean :
	-rm phonesup.o winsound1.o phonesup.dll $(PROGRAM).exe $(PROGRAM).o

%.dll : ${OBJS}
	dllwrap -o $*.dll --export-all-symbols --output-def $*.def $(OBJS) $(LDFLAGS) $(ALL_CFLAGS) $(LIBS)
	dllwrap -o $*.dll --def $*.def $(OBJS) $(LDFLAGS) $(ALL_CFLAGS) $(LIBS)


