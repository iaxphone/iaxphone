#include <stdio.h>

#include "assert.h"

int (*abort_function)(const char *, const char *, int);

int abort_function_c(const char *s, const char *file, int line) {

  printf("Aborting on (%s): File: %s, Line %d\n", s!=NULL ? s : "NULL", file!=NULL?file:"NULL", line);

  exit(1);

}

int set_abort_function(int (*func)(const char *, const char *, int)) {

  abort_function = func;

  return 0;

}
