#ifndef PHONE_PACK_H
#define PHONE_PACK_H

#include "iax_pack.h"
#include "audio_base.h"

struct phone_pack {
  void (*service_network)(int netfd, FILE *f, struct iax_pack *i, struct audio_base *a);
  int (*process_outqueue)(int netfd, FILE *f, struct iax_pack *i, struct audio_base *a);
  int (*process_audioin_prepare)(int netfd, FILE *f, struct iax_pack *i, struct audio_base *a);
  int (*process_audioin_process)(int netfd, FILE *f, struct iax_pack *i, struct audio_base *a);
  int (*create_thread)(int netfd, FILE *f, struct iax_pack *i, struct audio_base *a);
  int (*resume_thread)(void);
  int (*suspend_thread)(void);
  int (*parse_args)(FILE *f, char*, struct iax_pack *i, struct audio_base *a);
  void (*issue_prompt)(FILE *f);
  int (*initial_audio)(int,FILE*,struct iax_pack*, struct audio_base *a);

};

#endif
