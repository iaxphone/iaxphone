#ifndef IAX_BASIS_H
#define IAX_BASIS_H

#ifndef ASM_FORMAT_SPEEX
#define AST_FORMAT_SPEEX (1 << 9)
#endif

#ifndef AST_FORMAT_ILBC
#define AST_FORMAT_ILBC (1 << 10)
#endif

#ifdef MEMORY_SIZE
#error This file will no longer be needed when struct iax_session is defined in the matching iax.h of libiax2
#else
#define MEMORY_SIZE 1000
#define MAXSTRLEN 80
#endif

#ifdef WIN32
#include <windows.h>
#endif

#include "iax-client.h"

struct iax_session {
	/* Private data */
	void *pvt;
	/* Sendto function */
	sendto_t sendto;
	/* Is voice quelched (e.g. hold) */
	int quelch;
	/* Last received voice format */
	int voiceformat;
	/* Last transmitted voice format */
	int svoiceformat;
	/* Per session capability */
	int capability;
	/* Last received timestamp */
	unsigned int last_ts;
	/* Last transmitted timestamp */
	unsigned int lastsent;
	/* Last transmitted voice timestamp */
	unsigned int lastvoicets;
	/* Next predicted voice ts */
	unsigned int nextpred;
	/* True if the last voice we transmitted was not silence/CNG */
	int notsilenttx;
	/* Our last measured ping time */
	unsigned int pingtime;
	/* Address of peer */
	struct sockaddr_in peeraddr;
	/* Our call number */
	int callno;
	/* Peer's call number */
	int peercallno;
	/* Our next outgoing sequence number */
	unsigned char oseqno;
	/* Next sequence number they have not yet acknowledged */
	unsigned char rseqno;
	/* Our last received incoming sequence number */
	unsigned char iseqno;
	/* Last acknowledged sequence number */
	unsigned char aseqno;
	/* Peer supported formats */
	int peerformats;
	/* Time value that we base our transmission on */
	struct timeval offset;
	/* Time value we base our delivery on */
	struct timeval rxcore;
	/* History of lags */
	int history[MEMORY_SIZE];
	/* Current base jitterbuffer */
	int jitterbuffer;
	/* Informational jitter */
	int jitter;
	/* Measured lag */
	int lag;
	/* Current link state */
	int state;
	/* Peer name */
	char peer[MAXSTRLEN];
	/* Default Context */
	char context[MAXSTRLEN];
	/* Caller ID if available */
	char callerid[MAXSTRLEN];
	/* DNID */
	char dnid[MAXSTRLEN];
	/* Requested Extension */
	char exten[MAXSTRLEN];
	/* Expected Username */
	char username[MAXSTRLEN];
	/* Expected Secret */
	char secret[MAXSTRLEN];
	/* permitted authentication methods */
	char methods[MAXSTRLEN];
	/* MD5 challenge */
	char challenge[12];
#ifdef VOICE_SMOOTHING
	unsigned int lastts;
#endif
	/* Refresh if applicable */
	int refresh;
	
	/* Transfer stuff */
	struct sockaddr_in transfer;
	int transferring;
	int transfercallno;
	int transferid;
	int transferpeer;	/* for attended transfer */
	int transfer_moh;	/* for music on hold while performing attended transfer */

	/* For linking if there are multiple connections */
	struct iax_session *next;
};

#endif
