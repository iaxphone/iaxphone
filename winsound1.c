#include <windows.h>

#include <stdio.h>
#include "assert.h"

#include "winsound1.h"

WHOUT *outqueue;

/* audio input buffer headers */
WAVEHDR whin[NWHIN];

/* audio input buffers */
char bufin[NWHIN][SZE*sizeof(short)];

/* initialize the sequence variables for the audio in stuff */
unsigned int whinserial = 1,nextwhin = 1;

void CALLBACK waveOutProc(
  HWAVEOUT hwo,      
  UINT uMsg,         
  DWORD_PTR dwInstance,  
  DWORD dwParam1,    
  DWORD dwParam2) {

  FILE *f = (FILE*) dwInstance;

  //  wfprintf(f, "waveOutProc: \n");

  if (uMsg == WIM_DATA) {

    //wfprintf(f, " .. : WM_DATA\n");

  }

}

void CALLBACK waveInProc(
  HWAVEIN hwi,       
  UINT uMsg,         
  DWORD dwInstance,  
  DWORD dwParam1,    
  DWORD dwParam2     
  ) {

  struct rosen_pack *r = (struct rosen_pack *) dwInstance;

  //  wfprintf(r->f, "waveInProc: \n");

  if (uMsg == WIM_DATA) {

    //    wfprintf(r->f, " .. : WM_DATA\n");

  }

}

int det_outqueue(WHOUT *o) {
  
  int n = 0;

  if (o==NULL) return 0;
  
  while (o != NULL) {
    
    if (!(o->w.dwFlags & WHDR_DONE)) n++;

    o = o->next;

  }

  return n;

}

int throwaway_check(void *extra, int *n) {

  struct winsound1 *w = (struct winsound1 *) extra;
  int nval;

  assert(extra!=NULL);

  nval = det_outqueue(outqueue);

  if (n != NULL) *n = nval;

  return nval > OUT_DEPTH;

}

int block_audio(struct audio_base *a, int ms) {
  
  struct winsound1 *w;
  DWORD result;

  assert(a!=NULL && a->extra != NULL);

  w = (struct winsound1*) a->extra;

  result = MsgWaitForMultipleObjects(2, &w->audio_in, 0, 3, 0);

  return result;

}

int set_wfx_structures(WAVEFORMATEX *wfx) {

	assert(wfx!=NULL);

	ZeroMemory(wfx, sizeof(WAVEFORMATEX));
	wfx->wFormatTag = WAVE_FORMAT_PCM;
	wfx->nChannels = 1;
	wfx->wBitsPerSample = 16;
	wfx->nSamplesPerSec = 8000;
	wfx->nBlockAlign = (wfx->wBitsPerSample * wfx->nChannels) / 8;
	wfx->nAvgBytesPerSec = wfx->nSamplesPerSec * wfx->nBlockAlign;
	wfx->cbSize = 0;

	return 0;

	}

int init_audio(struct audio_base *a, void *cp) {

  WAVEFORMATEX wf;

  struct winsound1 *w;

  assert(a!=NULL && a->extra != NULL);

  w = (struct winsound1*) a->extra;

  w->audio_in = CreateEvent(NULL, TRUE, FALSE, "Audio Input");
  w->audio_out = CreateEvent(NULL, TRUE, FALSE, "Audio Output");

  set_wfx_structures(&wf);

  /* open the audio out channel */
  if (waveOutOpen(&w->wout,0,&wf,(DWORD_PTR)&w->audio_out,(DWORD_PTR)cp,CALLBACK_EVENT) != MMSYSERR_NOERROR)
    {
      wfprintf(stderr,"Fatal Error: Failed to open wave output device\n");
      return -1;
    }
  
  /* open the audio in channel */
  if (waveInOpen(&w->win,0,&wf,(DWORD_PTR)&w->audio_in,(DWORD_PTR)cp,CALLBACK_EVENT) != MMSYSERR_NOERROR)
    {
      wfprintf(stderr,"Fatal Error: Failed to open wave input device\n");
      waveOutReset(w->wout);
      waveOutClose(w->wout);
      return -1;
    }

return 0;

}

int close_audio(struct audio_base *a) {

  struct winsound1 *w;

  assert(a!=NULL);

  w = (struct winsound1*) a->extra;

  waveInReset(w->win);
  waveInClose(w->win);
  
  waveOutReset(w->wout);
  waveOutClose(w->wout);
  
  return 0;

}

int out_audio(void *extra, int n, short *fr, int fr_size, int *paused_xmit) {

  struct winsound1 *w = (struct winsound1*) extra;
  WHOUT *wh, *wh1;

  assert(extra!=NULL && fr!=NULL && paused_xmit != NULL);

  /* malloc the memory for the queue item */
  wh = (WHOUT *) malloc(sizeof(WHOUT));
  
  if (wh == (WHOUT *) NULL) /* if error, bail */
    {
      wfprintf(stderr,"Outa memory!!!!\n");
      return -1;
      
    }
  
  /* initialize the queue entry */
  memset(wh,0,sizeof(WHOUT));
  /* copy the PCM data from the gsm conversion buffer */
  memcpy((char *)wh->data,(char *)fr,fr_size);
  /* set parameters for data */
  wh->w.lpData = (char *) wh->data;
  wh->w.dwBufferLength = fr_size;

  /* prepare buffer for output */
  if (waveOutPrepareHeader(w->wout,&wh->w,sizeof(WAVEHDR)))
    {
      wfprintf(stderr,"Cannot prepare header for audio out\n");
      return -1;
      
    }

  /* if not currently transmitting, hold off a couple of packets for 
     smooth sounding output */
  if ((!n) && (!*paused_xmit))
    {
      /* pause output (before starting) */
      waveOutPause(w->wout);
      /* indicate as such */
      *paused_xmit = 1;
    }

  /* queue packet for output on audio device */
  if (waveOutWrite(w->wout,&wh->w,sizeof(WAVEHDR)))
    {
      
      wfprintf(stderr,"Cannot output to wave output device\n");
      return -1;
      
    }

  /* if we are paused, and we have enough packets, start audio */
  if ((n > OUT_PAUSE_THRESHOLD) && *paused_xmit)
    {
      /* start the output */
      waveOutRestart(w->wout);
      /* indicate as such */
      *paused_xmit = 0;
    }
  
  /* insert it onto tail of outqueue */
  if (outqueue == NULL) /* if empty queue */
    outqueue = wh; /* point queue to new entry */
  else /* otherwise is non-empty queue */
    {
      wh1 = outqueue;
      while(wh1->next) wh1 = wh1->next; /* find last entry in queue */
      wh1->next = wh; /* point it to new entry */
    }

  return 0;

}

int process_insound_prep(struct audio_base *a, int (*sn_callback)(void *, struct audio_base*), void *sn_callback_extra) {

  struct winsound1 *w;

  int i;

  assert(a!=NULL);

  w = (struct winsound1*) a->extra;

  /* go through all audio in buffers, and prepare and queue ones that are currently idle */
  for(i = 0; i < NWHIN; i++) {

    if (sn_callback!=NULL) sn_callback(sn_callback_extra, a);

    if (!(whin[i].dwFlags & WHDR_PREPARED)) /* if not prepared, do so */
	{
	  /* setup this input buffer header */
	  memset(&whin[i],0,sizeof(WAVEHDR));
	  whin[i].lpData = bufin[i];
	  whin[i].dwBufferLength = a->frame_size*sizeof(short);
	  whin[i].dwUser = whinserial++; /* set 'user data' to current serial number */

	  /* prepare the buffer */
	  if (waveInPrepareHeader(w->win,&whin[i],sizeof(WAVEHDR)))
	    {
	      wfprintf(stderr,"Unable to prepare header for input - prep waveInPrepare\n");
	      return -1;
	    }
	  
	  /* add it to device (queue) */
	  if (waveInAddBuffer(w->win,&whin[i],sizeof(WAVEHDR)))
	    {
	      wfprintf(stderr,"Unable to prepare header for input - prep waveInAdd\n");
	      return -1;
	    }
	}
      
      waveInStart(w->win); /* start it (if not already started) */

    }

  return 0;

}

int process_insound(struct audio_base *a, int (*sn_callback)(void *, struct audio_base*), int (*enc_callback)(void *, struct audio_base*, unsigned long*, int), void *sn_callback_extra, unsigned long *lastouttick, int answered_call) {

  struct winsound1 *w;
  int i;

  assert(a!=NULL && sn_callback!=NULL && enc_callback!=NULL && lastouttick != NULL);

  w = (struct winsound1*) a->extra;

  /* do audio input stuff for buffers that have received data from audio in device already. Must
     do them in serial number order (the order in which they were originally queued). */
  if(answered_call) /* send audio only if call answered */
    {

      for(;;) /* loop until all are found */
	{
	  for(i = 0; i < NWHIN; i++) /* find an available one that's the one we are looking for */
	    {
	      
	      if (sn_callback!=NULL) sn_callback(sn_callback_extra, a);

	      /* if not time to send any more, dont */
	      if (GetTickCount() < (*lastouttick + OUT_INTERVAL))
		{
		  i = NWHIN; /* set to value that WILL exit loop */
		  break;
		}
	      if ((whin[i].dwUser == nextwhin) && (whin[i].dwFlags & WHDR_DONE)) { /* if audio is ready */
		
		if (whin[i].dwBytesRecorded != whin[i].dwBufferLength)
		  {
		    wfprintf(stderr,"Short audio read, got %d bytes, expected %d bytes\n", whin[i].dwBytesRecorded,
			    whin[i].dwBufferLength);
		    return -1;
		  }

		enc_callback(sn_callback_extra, a, lastouttick, i);

		/* unprepare (free) the header */
		waveInUnprepareHeader(w->win,&whin[i],sizeof(WAVEHDR));
		/* initialize the buffer */
		memset(&whin[i],0,sizeof(WAVEHDR));
		/* bump the serial number to look for the next time */
		nextwhin++;

		/* exit the loop so that we can start at lowest buffer again */
		break;
	      }
	    } 

	  if (i >= NWHIN) break; /* if all found, get out of loop */

	}
    }

  return 0;

}

int process_outsound(struct audio_base *a, int (*sn_callback)(void *, struct audio_base*), void *sn_callback_extra) {

  WHOUT *wh,*wh1,*wh2;
  struct winsound1 *w;
  int i,c;

  assert(a!=NULL);

  w = (struct wndsound1*) a->extra;

  if (outqueue==NULL) return 0;

  /* go through audio output queue */
  for(wh = outqueue,wh1 = wh2 = NULL,i = 0; wh != NULL; wh = wh->next)
    {
      if (sn_callback!=NULL) sn_callback(sn_callback_extra, a);

      /* service network here for better performance */
				/* if last one was removed from queue, zot it here */
      if (i && wh1)
	{ 
	  free(wh1);
	  wh1 = wh2;
	}

      i = 0; /* reset "last one removed" flag */

      if (wh->w.dwFlags & WHDR_DONE) /* if this one is done */
	{
	  /* prepare audio header */
	  if ((c = waveOutUnprepareHeader(w->wout,&wh->w,sizeof(WAVEHDR))) != MMSYSERR_NOERROR)
	    { 
	      wfprintf(stderr,"Cannot unprepare audio out header, error %d\n",c);
	      return -1;
	    }
	  if (wh1 != NULL) /* if there was a last one */
	    {
	      wh1->next = wh->next;
	    } 
	  if (outqueue == wh) /* is first one, so set outqueue to next one */
	    {
	      outqueue = wh->next;
	    }
	  i = 1; /* set 'to free' flag */
	}

      wh2 = wh1;	/* save old,old wh pointer */
      wh1 = wh; /* save the old wh pointer */
    }

/* go through all audio in buffers, and prepare and queue ones that are currently idle */
for (i = 0; i < NWHIN; i++)
{
      if (sn_callback!=NULL) sn_callback(sn_callback_extra, a);

  /* service network stuff here for better performance */
  if (!(whin[i].dwFlags & WHDR_PREPARED)) /* if not prepared, do so */
    {
      /* setup this input buffer header */
      memset(&whin[i],0,sizeof(WAVEHDR));
      whin[i].lpData = bufin[i];
      whin[i].dwBufferLength = a->frame_size*sizeof(short);
      whin[i].dwUser = whinserial++; /* set 'user data' to current serial number */
      /* prepare the buffer */
      if (waveInPrepareHeader(w->win,&whin[i],sizeof(WAVEHDR)))
	{
	  wfprintf(stderr,"Unable to prepare header for input - prep process waveInPrep\n");
	  return -1;
	}
      /* add it to device (queue) */
      if (waveInAddBuffer(w->win,&whin[i],sizeof(WAVEHDR)))
	{
	  wfprintf(stderr,"Unable to prepare header for input - prep process waveInAdd\n");
	  return -1;
	}
    }

}

  waveInStart(w->win); /* start it (if not already started) */

  return 0;

}

char *fetch_wdata(int i) {

  assert(i<NWHIN);
  
  return whin[i].lpData;

}
