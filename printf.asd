(defsystem "printf"
  :description "printf from c.l.l"
  :version "0.95"
  :licence "public domain"
  :components ((:file "printf")))
