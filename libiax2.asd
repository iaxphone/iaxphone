(defsystem "libiax2"
  :description "libiax2 - Lisp IAX funcs / CFFI to IAX protocol library"
  :version "0.90"
  :author "Lester Vecsey"
  :licence "GPL"
  :depends-on (:cffi)
  :components ((:file "libiax2")))
