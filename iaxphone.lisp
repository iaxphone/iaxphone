
(in-package #:cl-user)

(defpackage #:iaxphone
  (:use #:cl #:cffi #:libiax2 #:ode #:phonesup)
  (:export #:start-run))

(in-package #:iaxphone)

;(declaim (optimize (speed 3) (safety 0) (compilation-speed 0) (debug 0)))

(defconstant +pif+ (coerce pi 'single-float))

(defvar *phi* (/ (+ 1 (sqrt 5)) 2))

(defvar *call-list* NIL)
(defvar *iax-accounts* NIL)

(defstruct iax-account
  (user 80 :type string)
  (pass 16 :type string)
  (host 120 :type string))

(defparameter *help* (list 
#"Welcome to the miniphone telephony client, the commands are as follows\n"
#"Help\t\t-\tDisplays this screen."
#"Call (Number) -\t-\tDials the number supplied."
#"Answer\t\t-\tAnswers an Inbound call."
#"Reject\t\t-\tRejects an Inbound call."
#"Dump\t\t-\tDumps (disconnects) the current call."
#"Dtmf (Digit)\t-\tSends specified DTMF digit."
#"Status\t\t-\tLists the current sessions and their current status."
#"Quit\t\t-\tShuts down the client."
""))

(defun draw-plate(width height depth)
  "Draw the phone plate."
  (gl:with-primitives :quads
    (gl:vertex 0 0 0)
    (gl:vertex width 0 0)
    (gl:vertex width height 0)
    (gl:vertex 0 height 0)))

(defun draw-origin(extent)
  "Draw the origin lines."
  (gl:with-primitives :lines
    (gl:material :front :ambient-and-diffuse #(1.0 0.0 0.0 0.5))    
    (gl:vertex 0 0 0)
    (gl:vertex extent 0 0)
    (gl:material :front :ambient-and-diffuse #(0.0 1.0 0.0 0.5))    
    (gl:vertex 0 0 0)
    (gl:vertex 0 extent 0)
    (gl:material :front :ambient-and-diffuse #(0.0 0.0 1.0 0.5))
    (gl:vertex 0 0 0)
    (gl:vertex 0 0 extent)))

(defun deg-to-rad(d)
  (/ (* 2 PI d) 360))

(defun draw-cube-outline(phi scale-factor)
  "Draw a cube outer wall outline."
  (declare (ignore scale-factor))
    (gl:shade-model :smooth)
    (gl:normal 0 0 1)
    (gl:enable :blend)
    (gl:Blend-Func :SRC-ALPHA  :ONE-MINUS-SRC-ALPHA)
    (gl:with-primitives :quad-strip 
      (gl:vertex 0 0 0)
      (gl:vertex 0 0 1)
      (gl:vertex 0 1 0)
      (gl:vertex 0 1 1)
      (gl:vertex 1 1 0)
      (gl:vertex 1 1 1)
      (gl:vertex 1 0 0)
      (gl:vertex 1 0 1)
      (gl:vertex 0 0 0)
      (gl:vertex 0 0 1))
    (gl::disable :blend))

(defun draw-cube-face(phi scale-factor)
  "Draw a cube textured face."
  (declare (ignore scale-factor))
    (gl:shade-model :smooth)
    (gl:normal 0 0 1)
    (gl:enable :blend)
    (gl:Blend-Func :SRC-ALPHA  :ONE-MINUS-SRC-ALPHA)
    (gl:with-primitives :quads
      (gl:tex-coord 0.0 0.0)
      (gl:vertex 0 0 1)
      (gl:tex-coord 1.0 0.0)
      (gl:vertex 1 0 1)
      (gl:tex-coord 1.0 1.0)
      (gl:vertex 1 1 1)
      (gl:tex-coord 0.0 1.0)
      (gl:vertex 0 1 1))
   (gl::disable :blend))

(defun draw-chessbox(phi scale-factor)
  "Draw a chessbox top."
  (declare (ignore scale-factor))
    (gl:shade-model :smooth)
    (gl:normal 0 0 1)
    (gl:enable :blend)
    (gl:Blend-Func :SRC-ALPHA  :ONE-MINUS-SRC-ALPHA)
;    (gl:translate 1.0 1.0 0.0)
    (gl:with-primitives :quad-strip 
	(gl:vertex -0.89 -0.11 0.00)
	(gl:vertex -0.96 -0.04 -0.20)
	(gl:vertex -0.89 -0.89 0.00)
	(gl:vertex -0.96 -0.96 -0.20)
	(gl:vertex -0.11 -0.89 0.00)
	(gl:vertex -0.04 -0.96 -0.20)
	(gl:vertex -0.11 -0.11 0.00)
	(gl:vertex -0.04 -0.04 -0.20)
	(gl:vertex -0.89 -0.11 0.00)
	(gl:vertex -0.96 -0.04 -0.20))
    (gl:with-primitives :quads
	(gl:vertex -0.89 -0.89 0.00)
	(gl:vertex -0.11 -0.89 0.00)
	(gl:vertex -0.11 -0.11 0.00)
	(gl:vertex -0.89 -0.11 0.00))
    (gl::disable :blend))

(defun draw-linebar(width length)
  "Draw a multi-use linebar object"
  (gl:shade-model :smooth)
  (gl:enable :blend)
  (gl:with-primitives :quad-strip
    (gl:vertex 0.0 0.0 0.0)
    (gl:vertex 0.0 width 0.0)
    (gl:vertex length 0.0 0.0)
    (gl:vertex length width 0.0)))

(defun draw-quasi-crystal(phi scale-factor)
  "Draw a Quasi Crystal."
  (declare (ignore scale-factor))
    (gl:shade-model :smooth)
    (gl:normal 0 0 1)
    (gl:enable :blend)
    (gl:Blend-Func :SRC-ALPHA  :ONE-MINUS-SRC-ALPHA)
      (let* ((phi-2 (/ phi 2)) (hyp (sqrt (+ 0.25 (* phi-2 phi-2))))
	     (vert (* hyp (sin (deg-to-rad 72))))
	     (mini (* hyp (sin (deg-to-rad 18)))))
	(setq mini 0)
;    (gl:translate 1.0 1.0 0.0)
    (gl:with-primitives :quad-strip 
	(gl:vertex -0.89 -0.11 0.00)
	(gl:vertex -0.96 -0.04 -0.20)
	(gl:vertex -0.89 -0.89 0.00)
	(gl:vertex -0.96 -0.96 -0.20)
	(gl:vertex -0.11 -0.89 0.00)
	(gl:vertex -0.04 -0.96 -0.20)
	(gl:vertex -0.11 -0.11 0.00)
	(gl:vertex -0.04 -0.04 -0.20)
	(gl:vertex -0.89 -0.11 0.00)
	(gl:vertex -0.96 -0.04 -0.20))
    (gl:with-primitives :quads
	(gl:vertex -0.89 -0.89 0.00)
	(gl:vertex -0.11 -0.89 0.00)
	(gl:vertex -0.11 -0.11 0.00)
	(gl:vertex -0.89 -0.11 0.00))
    (gl::disable :blend)))

;	(gl:vertex 0 0 0)
;	(gl:vertex phi-2 0 0.5)
;	(gl:vertex phi-2 0 -0.5)
;	(gl:vertex phi 0 0)
;	(gl:vertex (+ mini phi-2) vert -0.5)
;	(gl:vertex (+ mini phi) vert 0)
;	(gl:vertex phi-2 0 0.5)
;	(gl:vertex (+ mini phi-2) vert 0.5)

(defun iax-account->call(ia &optional extension)
  (with-slots (user pass host) ia
    (concatenate 'string user ":" pass "@" host
		 (cond ((eq NIL extension) "") (t (concatenate 'string "/" extension))))))

(defclass phone-window (glut:window)
  ((view-rotx :initform -30.0)
   (view-roty :initform 0.0)
   (view-rotz :initform 0.0)
   (angle :initform 0.0)
   (count :initform 1)
   (t0 :initform 0)
   us body mass joints amotors geoms spaces jointgroups colbundle contacts
   call-list
   texture-dtmf
   plx ply plz
   ps-i ps-a
   quasicrystal1-outline quasicrystal1-face origin plate linebar))

(defmethod initialize-instance :after ((window phone-window) &key)
  (with-slots (quasicrystal1-outline quasicrystal1-face origin plate linebar
				     call-list
				     texture-dtmf plx ply plz) window
    (gl:light :light0 :position #(5.0 5.0 10.0 0.0))
    (gl:enable :cull-face :lighting :light0 :depth-test)
    (setf texture-dtmf (gl:gen-textures 12))
    (prepare-textures texture-dtmf)
    ;; quasicrystal1
    (setf plx 5.0)
    (setf ply 6.5)
    (setf plz 0.5)
    (setf call-list *call-list*)
    (setq linebar (gl:gen-lists 1))
    (gl:with-new-list (linebar :compile)
      (gl:material :front :ambient-and-diffuse #(0.25 0.25 0.75 0.75))
      (draw-linebar 0.25 3))
    (setq quasicrystal1-outline (gl:gen-lists 1))
    (gl:with-new-list (quasicrystal1-outline :compile)
      (gl:material :front :ambient-and-diffuse #(1.0 1.0 0.0 0.75))
;      (draw-quasi-crystal *phi* 1))
					; (draw-chessbox *phi* 1)
      (draw-cube-outline *phi* 1))
    (setq quasicrystal1-face (gl:gen-lists 1))
    (gl:with-new-list (quasicrystal1-face :compile)
      (gl:material :front :ambient-and-diffuse #(1.0 1.0 0.0 0.75))
;      (draw-quasi-crystal *phi* 1))
					; (draw-chessbox *phi* 1)
      (draw-cube-face *phi* 1))
    (setq origin (gl:gen-lists 1))
    (gl:with-new-list (origin :compile)
      (draw-origin 5))
    (setq plate (gl:gen-lists 1))
    (gl:with-new-list (plate :compile)
      (gl:material :front :ambient-and-diffuse #(0.75 0.75 0.75 0.75))
      (draw-plate plx ply plz))
    (gl:enable :normalize)))

(defun print-frame-rate (window)
  "Prints the frame rate every ~5 seconds."
  (with-slots (count t0) window
    (incf count)
    (let ((time (get-internal-real-time)))
      (when (= t0 0)
        (setq t0 time))
      (when (>= (- time t0) (* 5 internal-time-units-per-second))
        (let* ((seconds (/ (- time t0) internal-time-units-per-second))
               (fps (/ count seconds)))
          (format *terminal-io* "~D frames in ~3,1F seconds = ~6,3F FPS~%"
                  count seconds fps))
        (setq t0 time)
        (setq count 0)))))

(defmethod glut:display ((window phone-window))
  (with-slots (view-rotx view-roty view-rotz angle
			 quasicrystal1-outline quasicrystal1-face origin plate linebar
			 texture-dtmf
			 us body mass joints amotors geoms spaces colbundle jointgroups)
      window
    (gl:clear :color-buffer-bit :depth-buffer-bit)
    (gl:with-pushed-matrix
      (gl:rotate view-rotx 1 0 0)
      (gl:rotate view-roty 0 1 0)
      (gl:rotate view-rotz 0 0 1)
      (gl:with-pushed-matrix
	  (gl:call-list plate))
      (gl:with-pushed-matrix
	  (gl:call-list origin))
      (loop for i from 0 to 11
	    do (let ((x (float (mod i 3))) (y (float (floor (/ i 3))))
		     (r (body-get-position (mem-aref body 'body-id i))))
		 (gl:with-pushed-matrix
		     (gl:translate (mem-aref r :float 0) (mem-aref r :float 1) (mem-aref r :float 2))
		   (gl:translate -0.5 -0.5 -0.5)
		   (gl:call-list quasicrystal1-outline)
		   (gl:bind-texture :texture-2d (nth (phone-map i 0) texture-dtmf))
		   (gl:enable :texture-2d)
		   (gl:tex-parameter :texture-2d :texture-min-filter :linear)
		   (gl:tex-parameter :texture-2d :texture-mag-filter :linear)
		   (gl:call-list quasicrystal1-face)
		   (gl:disable :texture-2d))))
      (gl:with-pushed-matrix
	  (gl:call-list linebar))
    (glut:swap-buffers)
    (update-amotors us body amotors)
    (collision-process us body amotors geoms spaces colbundle)
    (run-world us body mass joints amotors 1)
    (joint-group-empty (mem-aref jointgroups 'joint-group-id 0))
   (print-frame-rate window))))

(defmethod glut:idle ((window phone-window))
  (incf (slot-value window 'angle) 2.0)
  (glut:post-redisplay))

(defun dtmf(d i)
  (let ((mrs (most-recent-session)))
    (format t "mrs: ~a~%" mrs)
    (format t "Sending dtmf ~a~%" d)
    (iax-send-dtmf mrs d)))

(defvar fwd-map '(1 9 10 11 6 7 8 3 4 5))

(defun map-phone (i default)
  (car (nthcdr i fwd-map)))

(defvar rev-map '(0 0 0 7 8 9 4 5 6 1 2 3))

(defun phone-map (i default)
  (car (nthcdr i rev-map)))

(defun joint-work(body amotors index v)
  (declare (ignore amotors))
  (body-add-force (mem-aref body 'body-id (- (phone-map index 1) 1)) 0.0 0.0 (- (* 10 v))))
;  (joint-add-a-motor-torques (mem-aref amotors 'joint-id index) v v v))
;  (joint-set-a-motor-param (mem-aref amotors 'joint-id index) +PARAM-VEL-2+ v))

(defun dtmf-press(key body amotors index v)
  (body-add-force (mem-aref body 'body-id 
			    (case index
			      (#\* 0)
			      (#\# 2)
			      (t (map-phone index 0)))) 0.0 0.0 (- (* 10 v))))

(defmethod glut:keyboard ((window phone-window) key x y)
  (declare (ignore x y)) 
  (with-slots (body amotors ps-i ps-a call-list) window
    (let* ((v 0.2) 
	   (ia (car call-list))
	   (cs (concatenate 'string "call " (iax-account->call (car ia) (cdr ia)))))
      (format t "cs: ~a~%" cs)
  (case key
    (#\c (resume-thread) (parse-args NIL (foreign-string-alloc cs) ps-i ps-a))
    (#\* (dtmf 42 ps-i) (dtmf-press key body amotors #\* v))
    (#\# (dtmf 35 ps-i) (dtmf-press key body amotors #\# v))
    (#\0 (dtmf 48 ps-i) (dtmf-press key body amotors 0 (- v)))
    (#\1 (dtmf 49 ps-i) (dtmf-press key body amotors 1 v))
    (#\2 (dtmf 50 ps-i) (dtmf-press key body amotors 2 v))
    (#\3 (dtmf 51 ps-i) (dtmf-press key body amotors 3 v))
    (#\4 (dtmf 52 ps-i) (dtmf-press key body amotors 4 v))
    (#\5 (dtmf 53 ps-i) (dtmf-press key body amotors 5 v))
    (#\6 (dtmf 54 ps-i) (dtmf-press key body amotors 6 v))
    (#\7 (dtmf 55 ps-i) (dtmf-press key body amotors 7 v))
    (#\8 (dtmf 56 ps-i) (dtmf-press key body amotors 8 v))
    (#\9 (dtmf 57 ps-i) (dtmf-press key body amotors 9 v))
    (#\v   (glut:video-resize 50 50 800 60))
    (#\z (incf (slot-value window 'view-rotz) 5.0))
    (#\Z (decf (slot-value window 'view-rotz) 5.0))
    (#\Escape (parse-args NIL (foreign-string-alloc "QUIT") ps-i ps-a) (glut:leave-main-loop))))
  (glut:post-redisplay)))

(defmethod glut:special ((window phone-window) special-key x y)
  (declare (ignore x y))
  (with-slots (view-rotx view-roty) window
    (case special-key
      (:key-up (incf view-rotx 5.0))
      (:key-down (decf view-rotx 5.0))
      (:key-left (incf view-roty 5.0))
      (:key-right (decf view-roty 5.0)))
    (glut:post-redisplay)))

(defun number-mapping(x)
  (cond ((> x 1) (- x 2))
	(t (case x
	     (0 0)
	     (1 0)
	     (2 0)))))

(defmethod glut:reshape ((window phone-window) width height)
  (gl:viewport 0 0 width height)
  (gl:matrix-mode :projection)
  (gl:load-identity)
  (let ((h (/ height width)))
    (gl:frustum -1 1 (- h) h 5 60))
  (gl:matrix-mode :modelview)
  (gl:load-identity)
  (gl:translate -3 -3 -20))

(defmethod glut:visibility ((w phone-window) state)
  (case state
    (:visible (glut:enable-event w :idle))
    (t (glut:disable-event w :idle))))

(defcstruct colbundle
    (us :pointer)
  (geoms :pointer)
  (jointgroups :pointer)
  (contacts :pointer))

(defun lcd-finger(data x y pospack) 
  (let ((x1 (car pospack)) (y1 (cadr pospack)) (x2 (cadr (cdr pospack))) (y2 (car (nthcdr 3 pospack))))
    (loop for i from y1 to y2
	  do (loop for j from x1 to x2
		   do (setf (aref data (+ j (* i y))) 255)))))

(defun conv(h)
  (let ((s 0))
    (loop for x across h 
	  do (setf s (+ s (expt 2 (1+ x)))))
    s))

(defun make-pospack (x y finger)
  (let ((y10 (floor (/ y 10))) (x7 (floor (/ x 7))))
    (case finger
      (0 (list (* x7 2) (* y10 5) (* x7 3) (* y10 9)))
      (1 (list (* x7 2) (* y10 8) (* x7 6) (* y10 9)))
      (2 (list (* x7 5) (* y10 5) (* x7 6) (* y10 9)))
      (3 (list (* x7 2) (* y10 5) (* x7 6) (* y10 6)))
      (4 (list (* x7 2) (* y10 1) (* x7 3) (* y10 5)))
      (5 (list (* x7 2) (* y10 1) (* x7 6) (* y10 2)))
      (6 (list (* x7 5) (* y10 1) (* x7 6) (* y10 5))))))

(defvar compact-digits '(238 136 124 220 154 214 242 140 254 158))

(defun bit-expand(e)
  (loop for x from 1 to 8
	when (> (logand (expt 2 x) e) 0)
	collect (- x 1)))

(defun expand-digits (h)
  (loop for x across h
	collecting (bit-expand x)))

(defun run-expand-digits(o num)
  (expand-digits (make-array num :initial-contents o)))

(defun fill-e (data x y e)
  (cond ((not (null e)) (progn
			  (lcd-finger data x y (make-pospack x y (car e)))
			  (fill-e data x y (cdr e))))))

(defun fill-0 (data x y)
  (lcd-finger data x y (make-pospack x y 0))
  (lcd-finger data x y (make-pospack x y 1))
  (lcd-finger data x y (make-pospack x y 2))
  (lcd-finger data x y (make-pospack x y 4))
  (lcd-finger data x y (make-pospack x y 5))
  (lcd-finger data x y (make-pospack x y 6)))

(defun fill-x (data x y)
  (loop for i from 0 to (- y 1) by 4
	do (loop for j from 0 to (- x 1)
		 do (setf (aref data (+ j (* i y))) 255))))

(defun clear-data (data x y)
  (loop for i from 0 to (- y 1)
	do (loop for j from 0 to (- x 1)
		 do (setf (aref data (+ j (* i y))) 0))))

(defun prepare-textures (texture-dtmf)
  (let ((data (make-array (* 32 32))) (red (run-expand-digits compact-digits 10)))
    (loop for i from 0 to 9
	  do (progn
	       (clear-data data 32 32)
	       (fill-e data 32 32 (nth i red))
	       (gl:bind-texture :texture-2d (nth i texture-dtmf))
	       (format t "Bound texture: ~a~%" (nth i texture-dtmf))
	       (gl:enable :texture-2d)
	       (gl:tex-image-2d :texture-2d 0 :intensity 32 32 0 :luminance :unsigned-byte data)))))
    
(defun iaxphone(i a prompt num)
  (glut:init-display-mode :double :rgb :depth)
  (glut:init-window-size 800 600)
  (with-slots (us body mass joints amotors geoms spaces jointgroups colbundle contacts texture-dtmf ps-i ps-a)
	      (make-instance 'phone-window :title "iaxphone"
                 :events '(:visibility :reshape :special
                           :keyboard :display :idle))
    (setf us (world-create))
    (setf body (foreign-alloc 'body-id :count 12))
    (setf mass (foreign-alloc 'mass :count 12))
    (setf joints (foreign-alloc 'joint-id :count 11))
    (setf amotors (foreign-alloc 'joint-id :count 11))
    (setf geoms (foreign-alloc 'geom-id :count 14))
    (setf spaces (foreign-alloc 'space-id :count 1))
    (setf jointgroups (foreign-alloc 'joint-group-id :count 1))
    (setf colbundle (foreign-alloc 'colbundle :count 1))
    (setf contacts (foreign-alloc 'contact :count 13))
    (setf (foreign-slot-value colbundle 'colbundle 'jointgroups) jointgroups)
    (setf (foreign-slot-value colbundle 'colbundle 'geoms) geoms)
    (setf (foreign-slot-value colbundle 'colbundle 'us) us)
    (setf (foreign-slot-value colbundle 'colbundle 'contacts) contacts)
    (setf ps-i i)
    (setf ps-a a)
    (prepare-world us body mass joints amotors geoms spaces jointgroups 5.0 6.0 0.5)
    (initial-forces us body amotors)
    (glut:main-loop)))

(defvar sm-contact-bounce #x04)

(defcallback collision-callback :void ((data :pointer) (o1 :pointer) (o2 :pointer))
	     (let* ((d (pointer-address (mem-aref (foreign-slot-value data 'colbundle 'geoms) 'geom-id 0)))
		    (geoms (foreign-slot-value data 'colbundle 'geoms))
		    (gsurface (mem-aref geoms 'geom-id 0))
		    (jointgroups (foreign-slot-value data 'colbundle 'jointgroups))
		    (jointgroup (mem-aref jointgroups 'joint-group-id 0))
		   (us (foreign-slot-value data 'colbundle 'us))
		   (contacts (foreign-slot-value data 'colbundle 'contacts))
		   (cg (foreign-slot-pointer (mem-aref contacts 'contact 0) 'contact 'ode::geom)))
	       (let ((r (collide o1 o2 1 cg (foreign-type-size 'contact))))
		 (cond ((> r 0) (loop for i from 0 upto r 
;				      do (with-foreign-slots ((ode::mode ode::bounce ode::mu ode::bounce-vel) 
;							      (foreign-slot-value (mem-aref contacts 'contact i) 'contact 'ode::surface) surface-parameters-1)
;					(setf mode sm-contact-bounce)
;					(setf bounce 5.35))
;					(setf bounce-vel 30.0)
;					(setf mu 100.0))
				      do

				      (setf (foreign-slot-value (foreign-slot-value (mem-aref contacts 'contact i) 'contact 'ode::surface) 'surface-parameters 'ode::mode) sm-contact-bounce)
				      (setf (foreign-slot-value (foreign-slot-value (mem-aref contacts 'contact i) 'contact 'ode::surface) 'surface-parameters 'ode::bounce) 0.85)
				      (setf (foreign-slot-value (foreign-slot-value (mem-aref contacts 'contact i) 'contact 'ode::surface) 'surface-parameters 'ode::bounce-vel) 0.15)
;				      (setf (foreign-slot-value (foreign-slot-value (mem-aref contacts 'contact i) 'contact 'ode::surface) 'surface-parameters 'ode::mu) 31415923.0)
				      (let ((j (joint-create-contact us jointgroup (mem-aref contacts 'contact i))))
					(let ((g1 (cffi:null-pointer)) (g2 (cffi:null-pointer)))
					  (cond ((or (eq (pointer-address o1) gsurface) (eq (pointer-address o2) gsurface)) 
					       (setf g2 (geom-get-body (cond ((eq (pointer-address gsurface) o1) o2) (t o1)))))
						(t (progn
						     (setf g1 (geom-get-body o1))
						     (setf g2 (geom-get-body o2)))))
					  (joint-attach j g1 g2)))))))))

(defun map-pointers(o type names package)
  (map 'list #'(lambda (x) (setf (foreign-slot-value o type (find-symbol (string-upcase x) package)) (foreign-symbol-pointer x :code))) names))

(defun fill-audio-base(a extra)
  (let ((names (list "init_audio" "out_audio" "throwaway_check" "block_audio" 
		     "process_outsound" "process_insound_prep" "process_insound" "fetch_wdata" "close_audio")))
    (map-pointers a 'audio_base names 'phonesup)
    (setf (foreign-slot-value a 'audio_base 'phonesup::extra) extra)))

(defun fill-iax-pack(i netfd selected_format capabilities)
  (let ((names (list "iax_init" "iax_get_event" "iax_reject" "iax_accept" "iax_ring_announce" "iax_event_free"
		      "iax_time_to_next_event" "iax_hangup" "iax_answer" "iax_session_new" "iax_call"
		      "iax_send_dtmf" "iax_get_peer_addr" "iax_send_voice" "iax_destroy" "iax_get_fd")))
    (map-pointers i 'iax_pack names 'libiax2)
    (setf (foreign-slot-value i 'iax_pack 'libiax2::netfd) netfd)
    (setf (foreign-slot-value i 'iax_pack 'libiax2::selected_format) selected_format)
    (setf (foreign-slot-value i 'iax_pack 'libiax2::capabilities) capabilities)))

(defun set-phone-joints(w body mass joints amotors)
  (let ((min (float (- (/ 3.141592 4)))) (max (float (/ 3.141592 4))))
  (joint-attach (mem-aref joints 'joint-id 0) (mem-aref body 'body-id 0) (mem-aref body 'body-id 1))
  (joint-attach (mem-aref joints 'joint-id 1) (mem-aref body 'body-id 1) (mem-aref body 'body-id 2))
  (joint-attach (mem-aref joints 'joint-id 2) (mem-aref body 'body-id 3) (mem-aref body 'body-id 4))
  (joint-attach (mem-aref joints 'joint-id 3) (mem-aref body 'body-id 4) (mem-aref body 'body-id 5))
  (joint-attach (mem-aref joints 'joint-id 4) (mem-aref body 'body-id 6) (mem-aref body 'body-id 7))
  (joint-attach (mem-aref joints 'joint-id 5) (mem-aref body 'body-id 7) (mem-aref body 'body-id 8))
  (joint-attach (mem-aref joints 'joint-id 6) (mem-aref body 'body-id 9) (mem-aref body 'body-id 10))
  (joint-attach (mem-aref joints 'joint-id 7) (mem-aref body 'body-id 10) (mem-aref body 'body-id 11))
  (joint-attach (mem-aref joints 'joint-id 8) (mem-aref body 'body-id 1) (mem-aref body 'body-id 4))
  (joint-attach (mem-aref joints 'joint-id 9) (mem-aref body 'body-id 4) (mem-aref body 'body-id 7))
  (joint-attach (mem-aref joints 'joint-id 10) (mem-aref body 'body-id 7) (mem-aref body 'body-id 10))
  (joint-attach (mem-aref amotors 'joint-id 0) (mem-aref body 'body-id 0) (mem-aref body 'body-id 1))
  (joint-attach (mem-aref amotors 'joint-id 1) (mem-aref body 'body-id 1) (mem-aref body 'body-id 2))
  (joint-attach (mem-aref amotors 'joint-id 2) (mem-aref body 'body-id 3) (mem-aref body 'body-id 4))
  (joint-attach (mem-aref amotors 'joint-id 3) (mem-aref body 'body-id 4) (mem-aref body 'body-id 5))
  (joint-attach (mem-aref amotors 'joint-id 4) (mem-aref body 'body-id 6) (mem-aref body 'body-id 7))
  (joint-attach (mem-aref amotors 'joint-id 5) (mem-aref body 'body-id 7) (mem-aref body 'body-id 8))
  (joint-attach (mem-aref amotors 'joint-id 6) (mem-aref body 'body-id 9) (mem-aref body 'body-id 10))
  (joint-attach (mem-aref amotors 'joint-id 7) (mem-aref body 'body-id 10) (mem-aref body 'body-id 11))
  (joint-attach (mem-aref amotors 'joint-id 8) (mem-aref body 'body-id 1) (mem-aref body 'body-id 4))
  (joint-attach (mem-aref amotors 'joint-id 9) (mem-aref body 'body-id 4) (mem-aref body 'body-id 7))
  (joint-attach (mem-aref amotors 'joint-id 10) (mem-aref body 'body-id 7) (mem-aref body 'body-id 10))
  (joint-set-ball-anchor (mem-aref joints 'joint-id 0) 1.0 0.5 0.0)
  (joint-set-ball-anchor (mem-aref joints 'joint-id 1) 2.0 0.5 0.0)
  (joint-set-ball-anchor (mem-aref joints 'joint-id 2) 1.0 1.5 0.0)
  (joint-set-ball-anchor (mem-aref joints 'joint-id 3) 2.0 1.5 0.0)
  (joint-set-ball-anchor (mem-aref joints 'joint-id 4) 1.0 2.5 0.0)
  (joint-set-ball-anchor (mem-aref joints 'joint-id 5) 2.0 2.5 0.0)
  (joint-set-ball-anchor (mem-aref joints 'joint-id 6) 1.0 3.5 0.0)
  (joint-set-ball-anchor (mem-aref joints 'joint-id 7) 2.0 3.5 0.0)
  (joint-set-ball-anchor (mem-aref joints 'joint-id 8) 1.5 1.0 0.0)
  (joint-set-ball-anchor (mem-aref joints 'joint-id 9) 1.5 2.0 0.0)
  (joint-set-ball-anchor (mem-aref joints 'joint-id 10) 1.5 3.0 0.0)
  (loop for i from 0 to 10
	do (progn
	     (joint-set-a-motor-mode (mem-aref amotors 'joint-id i) +A-MOTOR-USER+)
	     (joint-set-a-motor-axis (mem-aref amotors 'joint-id i) 0 1 0.0 1.0 0.0)
	     (joint-set-a-motor-num-axes (mem-aref amotors 'joint-id i) 1)
	     (joint-set-a-motor-param (mem-aref amotors 'joint-id i) +PARAM-F-MAX+ 5.0)
	     (joint-set-a-motor-param (mem-aref amotors 'joint-id i) +PARAM-VEL-2+ 3.0)
	     (joint-set-a-motor-param (mem-aref amotors 'joint-id i) +PARAM-VEL-3+ 4.0)
	     (joint-set-a-motor-param (mem-aref amotors 'joint-id i) +PARAM-LO-STOP+ min)
	     (joint-set-a-motor-param (mem-aref amotors 'joint-id i) +PARAM-HI-STOP+ max)))))

(defun prepare-joints(w body mass joints amotors)
  (loop for i from 0 to 10
	do (progn
	     (setf (mem-aref joints 'joint-id i) (joint-create-ball w NIL))
	     (setf (mem-aref amotors 'joint-id i) (joint-create-a-motor w NIL)))))

;  (set-phone-joints w body mass joints amotors))

(defun prepare-world(w body mass joints amotors geoms spaces jointgroups plx ply plz)
  (world-set-gravity w 0.0 0.0 -0.05) ; -9.81
  (let ((space (hash-space-create (cffi:null-pointer))))
    (let ((gsurface (create-box (cffi:null-pointer) plx ply plz)) (gtransform (create-geom-transform space)))
      (setf (mem-aref jointgroups 'joint-group-id 0) (joint-group-create 0))
      (setf (mem-aref geoms 'geom-id 1) gsurface)
      (setf (mem-aref geoms 'geom-id 0) gtransform)
      (geom-transform-set-geom gtransform gsurface)
      (geom-transform-set-info gtransform 1)
      (geom-set-position gtransform (/ plx 2) (/ ply 2) (/ plz 2)))
    (loop for i from 2 to 13
	  do (let ((gitem (create-box space 1.0 1.0 1.0)))
	       (setf (mem-aref geoms 'geom-id i) gitem)))
    (setf (mem-aref spaces 'space-id 0) space))
  (loop for i from 0 to 11
	do (progn
	     (setf (mem-aref body 'body-id i) (body-create w))
	     (let ((x (float (mod i 3))) (y (float (floor (/ i 3)))) (z (float 1)))
	       (body-set-position (mem-aref body 'body-id i) (+ 1.0 (* 1.5 x)) (+ 1.0 (* 1.5 y)) (+ 1.0 (* 1.5 z)))
	       (mass-set-zero (mem-aref mass 'mass i))
	       (mass-set-box (mem-aref mass 'mass i) 1.0 *phi* *phi* *phi*)
	       (mass-set-box-total (mem-aref mass 'mass i) 5.0 *phi* *phi* *phi*))
	     (geom-set-body (mem-aref geoms 'geom-id (+ 2 i)) (mem-aref body 'body-id i))))
  (prepare-joints w body mass joints amotors))

(defun print-body(body index)
  (let ((res (body-get-position (mem-aref body 'body-id index))))
    (let ((x (mem-aref res :float 0)) (y (mem-aref res :float 1)) (z (mem-aref res :float 2)))
  (format t "~a: ~f ~f ~f~%" index x y z))))

(defun initial-forces(w body amotors)
  (declare (ignore body))
  (joint-set-a-motor-param (mem-aref amotors 'joint-id 1) +PARAM-VEL-2+ 0.3))

(defun update-amotors(w body amotors)
  (declare (ignore w body))
  (loop for i from 0 to 10
	do (progn
	     (joint-set-a-motor-param (mem-aref amotors 'joint-id i) +PARAM-VEL-2+ 3.0)
	     (joint-set-a-motor-param (mem-aref amotors 'joint-id i) +PARAM-VEL-3+ 4.0))))

(defun collision-process(w body amotors geoms spaces colbundle)
  (declare (ignore w body amotors geoms))
  (space-collide (mem-aref spaces 'space-id 0) colbundle (callback collision-callback)))

(defun run-world(w body mass joints amotors num)
  (declare (ignore body mass amotors joints))
  (loop for i from 0 to num
	do (progn
	     (world-step w 0.2))))

(defun show-lines(h)
  (map 'list (lambda (x) (format t (printf:printf->format (concatenate 'string x #"\n")))) h))

(defun process-lines (i a prompt-string retval)
  (cond ((not (eq retval PS-QUIT)) 
	 (progn (format t prompt-string)
		(let ((statement (read-line)))
		  (cond ((string< "call " statement) (resume-thread)))
		  (cond ((string= "help" statement) (show-lines *help*)))
		  (cond ((string= "resume" statement) (resume-thread)))
		  (process-lines i a prompt-string (parse-args NIL (foreign-string-alloc statement) i a)))))))

(defun terminal(func)
  (defcallback abort-function :int ((s :pointer) (f :pointer) (line :int))
	     (format t "Aborting on (~a): File: ~a, Line ~a"
	     (foreign-string-to-lisp s) (foreign-string-to-lisp f) line)
	     (return-from terminal))
  (defcallback stroutput :int ((s :pointer) (len :int) (extra :pointer))
	       (format t "~a" (foreign-string-to-lisp s len))
	       0)
  (set-stroutput (callback stroutput))
  (set-abort-function (callback abort-function))
  (format t "iax-init: ~a~%" (iax-init 0))
  (let ((netfd (iax-get-fd)))
    (with-foreign-object (a 'audio_base)
      (with-foreign-object (w1 'winsound1)
	(with-foreign-object (i 'iax_pack)
	  (fill-iax-pack i netfd libiax2::AST_FORMAT_SPEEX libiax2::AST_FORMAT_SPEEX)
	  (fill-audio-base a w1)
	  (cond ((eq -1 (initial-audio netfd NIL i a)) (return-from terminal)))
	  (cond ((not (eq 0 (process-audioin-prepare netfd NIL i a))) (return-from terminal)))
	  (cond ((not (eq 0 (create-thread netfd NIL i a))) (return-from terminal)))
	  (funcall func i a "TeleClient> " 0))))))

(defun start-iax(&optional gl-yes)
  (cond ((eq -1 (phonesup-init)) (return-from start-iax)))
  (terminal (cond ((eq t gl-yes) #'iaxphone) (t #'process-lines)))
  (phonesup-cleanup))

