(defsystem "phonesup"
  :description "phonesup - miniphone terminal and audio support functions for libiax2"
  :version "0.90"
  :author "Lester Vecsey"
  :licence "GPL"
  :depends-on (:cffi :libiax2)
  :components ((:file "phonesup")))
