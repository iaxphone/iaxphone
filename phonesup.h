#ifndef PHONESUP_H
#define PHONESUP_H

#define MAXARGS 10
#define MAXARG 256

#define PS_QUIT 32

#ifdef WIN32
#include <windows.h>
#endif

#include "iax_basis.h"

#include "iax-client.h"

#include "iax_pack.h"

struct rosen_pack {

  int netfd;
  FILE *f;
  struct iax_pack *i;

};

extern struct rosen_pack rp;

struct thread_pack {

  struct rosen_pack *r;
  struct audio_base *a;
  unsigned long state;

#ifdef WIN32
  DWORD dwThreadId;
#endif

};

#define tr_process 0x1

extern struct thread_pack thr;
extern HANDLE Process_Thread;

#endif
