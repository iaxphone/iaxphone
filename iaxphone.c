#include <stdio.h>
#include "assert.h"

#ifdef WIN32
#include <windows.h>
#endif

#include <iax.h>

#include "iax_basis.h"

#include "iax_pack.h"

#include "phonesup.h"

#include <readline/readline.h>

#include "phone_pack.h"

#include "audio_base.h"

#ifdef WIN32
#ifdef WINSOUND1
#include "winsound1.h"
#endif
#endif

int function_fill( void **fill, char **s, char **e, HANDLE h) {

  assert(fill != NULL && s != NULL && e != NULL && s < e);

  while ( s < e ) {

    if ( (*fill++ = GetProcAddress(h, *s++)) == NULL) return -1;

  }

  return 0;

}

int gen_fill(void **fill, char *stringname, char *num_funcname, 
	     HMODULE numinfo, HMODULE actual) {

  char **pack_strings = (char**) GetProcAddress(numinfo, stringname),
    **s, **e;

  int (*num_pack_strings)(void) = (int (*)(void)) GetProcAddress(numinfo, num_funcname);

  assert(fill!=NULL);

  if (pack_strings==NULL || num_pack_strings==NULL) return -1;
  
  return function_fill( fill, pack_strings, pack_strings + num_pack_strings(), actual);

}

void dump_array(FILE *f, char **array) {

  assert(f!=NULL && array!=NULL);
  
  while(*array)
    fprintf(f, "%s\n", *array++);
  
  fflush(f);

}

static char *help[] = {
"Welcome to the miniphone telephony client, the commands are as follows:\n",
"Help\t\t-\tDisplays this screen.",
"Call <Number>\t-\tDials the number supplied.",
"Answer\t\t-\tAnswers an Inbound call.",
"Reject\t\t-\tRejects an Inbound call.",
"Dump\t\t-\tDumps (disconnects) the current call.",
"Dtmf <Digit>\t-\tSends specified DTMF digit.",
"Status\t\t-\tLists the current sessions and their current status.",
"Quit\t\t-\tShuts down the client.",
"",
0
};

int process_lines(struct phone_pack *p, int netfd, FILE *f, struct iax_pack *i, struct audio_base *a, char *call_list) {

  char *line;
  int retval = 0, len;

  assert(p!=NULL && i!=NULL);

  while ( retval!=PS_QUIT && (line = readline("TeleClient> ")) != NULL) {
    
    len = strlen(line);

    printf("len=%d\n", len);

    if (*line) add_history(line);

    printf("call_list = %p\n", call_list);
    if (call_list!=NULL) printf("call_list: %s\n", call_list);

    if (len>=9 && !strncmp(line, "call-list", 9) && call_list!=NULL) {

      char string[80];
      if (len+7 > sizeof(string)) continue;
      sprintf(string, "call %s", call_list);
      printf("Sending: %s\n", string);
      retval = p->parse_args(f, string, i, a);

    }

    else

    if (len>=4 && !strncmp(line, "help", 4)) {

      dump_array(stdout, help);
    
    }

    else {

      retval = p->parse_args(f, line, i, a);

    }

    free(line);

  }

  return 0;

}

int main(int argc, char *argv[]) {

  char *call_list = getenv("IAXPHONE_CALL_LIST");

#ifdef WINSOUND1
  struct winsound1 w1;
  void *audio_base_extra = &w1;
#endif

  struct iax_pack i;
  struct phone_pack p;
  struct audio_base a; 
  WSADATA wsaData;
  HMODULE h = LoadLibrary("libiax2.dll"), 
    hp = LoadLibrary("phonesup.dll");
  
  FILE *f;

  if (h==NULL || hp==NULL) return -1;

#ifdef CL_ABORT
  set_abort_function(abort_function_c);
#endif

  a.extra = audio_base_extra;

  if (gen_fill(&i.iax_init, "iax_pack_strings", "num_iax_pack_strings",
	       hp, h) == -1) return -1;

  if (gen_fill(&p.service_network, "phone_pack_strings", "num_phone_pack_strings",
	       hp, hp) == -1) return -1;

  if (gen_fill(&a.init_audio, "audio_base_strings", "num_audio_base_strings",
	       hp, hp) == -1) return -1;
	
  if (WSAStartup(MAKEWORD(2,2),&wsaData)) {
    return -1;
  }

  i.iax_init(0);

  i.netfd = i.iax_get_fd();
  
  // i.selected_format = AST_FORMAT_ILBC;
  // i.selected_format = AST_FORMAT_SPEEX;
  i.selected_format = AST_FORMAT_GSM;

  // i.capabilities = AST_FORMAT_ILBC;
  // i.capabilities = AST_FORMAT_SPEEX;
  i.capabilities = AST_FORMAT_GSM;

  f = stdout;
  _dup2(fileno(stdout),fileno(stderr));

  if (p.initial_audio(i.netfd,f,&i,&a) == -1) return -1;
	
  p.process_audioin_prepare(i.netfd,f, &i, &a);

  p.create_thread(i.netfd,f,&i,&a);
  p.resume_thread();
  process_lines(&p, i.netfd, f, &i, &a, call_list);

//	i.iax_destroy();

	WSACleanup();
	
  return 0;

}
