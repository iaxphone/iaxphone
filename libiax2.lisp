(in-package "COMMON-LISP-USER")

(defpackage #:libiax2
  (:use #:common-lisp #:CFFI)
  (:export :iax-init
	   :iax_pack
	   :iax-send-dtmf
	   :AST_FORMAT_GSM
	   :AST_FORMAT_SPEEX
	   :AST_FORMAT_ILBC
	   :iax-get-fd))

(in-package "LIBIAX2")

(defvar AST_FORMAT_GSM #x2)
(defvar AST_FORMAT_SPEEX #x200)
(defvar AST_FORMAT_ILBC #x400)

;(define-foreign-library libiax2
;  (:darwin (:or "libiax2.dylib" "libiax2.3.dylib" #-(and)(:framework "GLUT")))
;  (:windows "libiax2.dll")
;  (:unix (:or "libiax2.so" "libiax2.so.3")))

;(define-foreign-library libiax2
;    (:windows "c:/code/clisp/iaxphone/libiax2.dll"))

(define-foreign-library libiax2
    (:windows "c:/code/clisp/iaxphone/libiax2-debug.dll"))

(use-foreign-library libiax2)

(defcstruct iax_pack
    "Bundle of pointers to currently loaded libiax2 library functions."
  (iax_init :pointer)
  (iax_get_event :pointer)
  (iax_reject :pointer)
  (iax_accept :pointer)
  (iax_ring_announce :pointer)
  (iax_event_free :pointer)
  (iax_time_to_next_event :pointer)
  (iax_hangup :pointer)
  (iax_answer :pointer)
  (iax_session_new :pointer)
  (iax_call :pointer)
  (iax_send_dtmf :pointer)
  (iax_get_peer_addr :pointer)
  (iax_send_voice :pointer)
  (iax_destroy :pointer)
  (iax_get_fd :pointer)
  (netfd :int)
  (selected_format :int)
  (capabilities :int))
 
;  int (*iax_init)(int preferredportno);
;  struct iax_event *(*iax_get_event)(int blocking);
;  int (*iax_reject)(struct iax_session *session, char *reason);
;  int (*iax_accept)(struct iax_session *session, int format);
;  int (*iax_ring_announce)(struct iax_session *session);
;  int (*iax_event_free)(struct iax_event *event);
;  int (*iax_time_to_next_event)(void);
;  int (*iax_hangup)(struct iax_session *session, char *byemsg);
;  int (*iax_answer)(struct iax_session *session);
;  int (*iax_session_new)(void);
;  int (*iax_call)(struct iax_session *session, char *cidnum, char *cidname, char *ich, char *lang, int wait, int format, int capability);
;  int (*iax_send_dtmf)(struct iax_session *session, char digit);
;  struct sockaddr_in (*iax_get_peer_addr)(struct iax_session *session);
;  int (*iax_send_voice)(struct iax_session *session, int format, char *data, int datalen, int samples);
;  int (*iax_get_fd)(void);
 
;  int netfd,selected_format,capabilities;

(defcfun "iax_init" :int
  (preferredportno :int))

(defcfun "iax_send_dtmf" :int
  (session :pointer)
  (digit :char))

(defcfun "iax_get_fd" :int)

