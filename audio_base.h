#ifndef AUDIO_BASE_H
#define AUDIO_BASE_H

#ifdef ILBC_ENABLE
#define SZE 240
#else
#define SZE 160
#endif

typedef struct whout {
	WAVEHDR w;
	short	data[SZE];
	struct whout *next;
} WHOUT;

struct audio_base {

  int (*init_audio)(struct audio_base *a, void *callback_pointer);
  int (*out_audio)(void *extra, int n, short * fr, int fr_size, int *paused_xmit);
  int (*throwaway_check)(void *extra, int *n);
  int (*block_audio)(struct audio_base *a, int ms);
  int (*process_outsound)(struct audio_base *a, int (*sn_callback)(void *, struct audio_base*), void *sn_callback_extra);
  int (*process_insound_prep)(struct audio_base *a, int (*sn_callback)(void *, struct audio_base*), void *sn_callback_extra);
  int (*process_insound)(struct audio_base *a, int (*sn_callback)(void *, struct audio_base*), int (*enc_callback)(void *, struct audio_base*, unsigned long *longlasttick, int i), void *sn_callback_extra, unsigned long *lastouttick, int answered_call);
  char * (*fetch_wdata)(int i);
  int (*close_audio)(struct audio_base *a);

  void *extra;
  int frame_size;

};

#endif
