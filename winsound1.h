#ifndef WINSOUND1_H
#define WINSOUND1_H

#include <stdio.h>
#include <windows.h>

#include "audio_base.h"

struct winsound1 {

  HANDLE audio_in, audio_out;
  WHOUT *wh;

  HWAVEOUT wout;
  HWAVEIN win;

};

/* parameters for audio in */
#define	NWHIN 8				/* number of input buffer entries */
/* NOTE the OUT_INTERVAL parameter *SHOULD* be more around 18 to 20 or so, since the packets should
be spaced by 20 milliseconds. However, in practice, especially in Windoze-95, setting it that high
caused underruns. 10 is just ever so slightly agressive, and the receiver has to chuck a packet
every now and then. Thats about the way it should be to be happy. */
#define	OUT_INTERVAL 10		/* number of ms to wait before sending more data to peer */
/* parameters for audio out */
#define	OUT_DEPTH 12		/* number of outbut buffer entries */
#define	OUT_PAUSE_THRESHOLD 2 /* number of active entries needed to start output (for smoothing) */

extern WHOUT *outqueue;
extern WAVEHDR whin[NWHIN];
extern char bufin[NWHIN][SZE*sizeof(short)];
extern unsigned int whinserial,nextwhin;
extern HWAVEOUT wout;
extern HWAVEIN win;

int out_audio(void *extra, int n, short *fr, int fr_size, int *paused_xmit);

#endif
