(in-package "COMMON-LISP-USER")

(defpackage #:phonesup
  (:use #:common-lisp #:CFFI #:libiax2)
  (:export :service-network
	   :initial-audio
	   :create-thread
	   :resume-thread
	   :suspend-thread
	   :issue-prompt
	   :phonesup-init
	   :phonesup-cleanup
	   :set-stroutput
	   :set-any-keyboard-data
	   :set-grab-line
	   :parse-args
	   :process-outqueue
	   :phone_pack
	   :audio_base
	   :winsound1
	   :set-abort-function
	   :most-recent-session
	   :PS-QUIT
	   :*iax-pack-strings*
	   :*phone-pack-strings*
	   :num-iax-pack-strings
	   :num-phone-pack-strings
	   :process-audioin-prepare
	   :process-audioin-process))

(in-package "PHONESUP")

;(define-foreign-library libiax2
;  (:darwin (:or "libiax2.dylib" "libiax2.3.dylib" #-(and)(:framework "GLUT")))
;  (:windows "libiax2.dll")
;  (:unix (:or "libiax2.so" "libiax2.so.3")))

(define-foreign-library phonesup
    (:windows "c:/code/clisp/iaxphone/phonesup.dll"))

(defparameter PS-QUIT 32) 

(use-foreign-library phonesup)

(defcstruct phone_pack
    (service_network :pointer)
  (process_outqueue :pointer)
  (process_audioin_prepare :pointer)
  (process_audioin_process :pointer)
  (create_thread :pointer)
  (resume_thread :pointer)
  (suspend_thread :pointer)
  (parse_args :pointer)
  (issue_prompt :pointer)
  (initial_audio :pointer))

(defcstruct audio_base
    (init_audio :pointer)
  (out_audio :pointer)
  (throwaway_check :pointer)
  (block_audio :pointer)
  (process_outsound :pointer)
  (process_insound_prep :pointer)
  (process_insound :pointer)
  (fetch_wdata :pointer)
  (close_audio :pointer)
  (extra :pointer)
  (frame_size :int))

(defcstruct winsound1
  (audio_in :pointer)
  (audio_out :pointer)
  (wh :pointer)
  (win :pointer)
  (wout  :pointer))

(defcfun "sample_out" :int)

(defcfun "set_abort_function" :int
  (abort_func :pointer))

(defcfun "init_audio" :int
  (a :pointer)
  (callback_pointer :pointer))

(defcfun "out_audio" :int
  (extra :pointer)
  (n :int)
  (fr :pointer)
  (fr_size :int)
  (paused_xmit :pointer))

(defcfun "throwaway_check" :int
  (extra :pointer)
  (n :pointer))

(defcfun "block_audio" :int
  (a :pointer)
  (ms :int))

(defcfun "process_outsound" :int
  (a :pointer)
  (sn_callback :pointer)
  (sn_callback_extra :pointer))

(defcfun "process_insound_prep" :int
  (a :pointer)
  (sn_callback :pointer)
  (sn_callback_extra :pointer))

(defcfun "process_insound" :int
  (a :pointer)
  (sn_callback :pointer)
  (enc_callback :pointer)
  (sn_callback_extra :pointer)
  (lastouttick :pointer)
  (answered_call :int))

(defcfun "close_audio" :int
  (a :pointer))

(defcfun "initial_audio" :int
  (netfd :int)
  (file :pointer)
  (i :pointer)
  (a :pointer))

(defcfun "create_thread" :int
  (netfd :int)
  (file :pointer)
  (i :pointer)
  (a :pointer))

(defcfun "resume_thread" :int)
(defcfun "suspend_thread" :int)

(defcfun "most_recent_session" :pointer)

(defcfun "set_stroutput" :int
  (stroutput :pointer))

(defcfun "phonesup_init" :int)

(defcfun "phonesup_cleanup" :int)

(defcfun "parse_args" :int
  (file :pointer)
  (cmd :pointer)
  (i :pointer)
  (a :pointer))

(defcfun "service_network" :int
  (netfd :int)
  (file :pointer)
  (i :pointer)
  (a :pointer))

(defcfun "process_outqueue" :int
  (netfd :int)
  (file :pointer)
  (i :pointer)
  (a :pointer))

(defcfun "process_audioin_prepare" :int
  (netfd :int)
  (file :pointer)
  (i :pointer)
  (a :pointer))

(defcfun "issue_prompt" :int
  (file :pointer))

(defcvar "iax_pack_strings" :pointer :read-only t)
(defcvar "phone_pack_strings" :pointer :read-only t)

(defcfun "num_iax_pack_strings" :int)
(defcfun "num_phone_pack_strings" :int)

(defcfun "process_audioin_process" :int
  (netfd :int)
  (file :pointer)
  (i :pointer)
  (a :pointer))
