#ifndef IAX_PACK_H
#define IAX_PACK_H

#include <stdio.h>

#ifdef WIN32
#include <windows.h>
#endif

struct iax_pack {
  
  int (*iax_init)(int preferredportno);
  struct iax_event *(*iax_get_event)(int blocking);
  int (*iax_reject)(struct iax_session *session, char *reason);
  int (*iax_accept)(struct iax_session *session, int format);
  int (*iax_ring_announce)(struct iax_session *session);
  int (*iax_event_free)(struct iax_event *event);
  int (*iax_time_to_next_event)(void);
  int (*iax_hangup)(struct iax_session *session, char *byemsg);
  int (*iax_answer)(struct iax_session *session);
  struct iax_session* (*iax_session_new)(void);
  int (*iax_call)(struct iax_session *session, char *cidnum, char *cidname, char *ich, char *lang, int wait, int format, int capability);
  int (*iax_send_dtmf)(struct iax_session *session, char digit);
  struct sockaddr_in (*iax_get_peer_addr)(struct iax_session *session);
  int (*iax_send_voice)(struct iax_session *session, int format, char *data, int datalen, int samples);
  int (*iax_destroy)(struct iax_session *session);
  int (*iax_get_fd)(void);
 
  int netfd, selected_format, capabilities;

	};

extern char *iax_pack_strings[];

#endif
