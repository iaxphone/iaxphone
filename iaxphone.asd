(defsystem "iaxphone"
  :description "iaxphone - OpenGL voice over IP using IAX protocol"
  :version "0.90"
  :author "Lester Vecsey"
  :licence "GPL"
  :depends-on (:cffi :cl-opengl :cl-glut :cl-ode :libiax2 :phonesup :printf)
  :components ((:file "iaxphone")))

